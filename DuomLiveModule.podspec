#
# Be sure to run `pod lib lint DuomLiveModule.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'DuomLiveModule'
    s.version          = '1.0.1'
    s.summary          = 'A short description of DuomLiveModule.'
    
    # This description is used to generate tags and improve search results.
    #   * Think: What does it do? Why did you write it? What is the focus?
    #   * Try to keep it short, snappy and to the point.
    #   * Write the description between the DESC delimiters below.
    #   * Finally, don't worry about the indent, CocoaPods strips it!
    
    s.description      = <<-DESC
    TODO: Add long description of the pod here.
    DESC
    
    s.homepage         = 'https://gitlab.com/gwave_mobile_public/duomlivemodule'
    # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'HeKai_Duom' => 'kai.he@duom.com'  }
    s.source           = { :git => 'https://gitlab.com/gwave_mobile_public/duomlivemodule.git', :tag => s.version.to_s }
    # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
    
    s.ios.deployment_target = '12.0'
    s.swift_version = '5.0'
    
    s.resources    = 'DuomLiveModule/Assets/*', 'DuomLiveModule/LottieSources/*', 'DuomLiveModule/VideoSources/*'
    
    s.subspec 'API' do |spec|
        spec.source_files = 'DuomLiveModule/Classes/API/**/*'
    end
    
    s.subspec 'Model' do |spec|
        spec.source_files = 'DuomLiveModule/Classes/Model/**/*'
    end
    
    s.subspec 'Shared' do |spec|
        spec.source_files = 'DuomLiveModule/Classes/Shared/**/*'
    end
    
    s.subspec 'ViewControllers' do |spec|
        spec.source_files = 'DuomLiveModule/Classes/ViewControllers/**/*'
    end
    
    s.subspec 'ViewModel' do |spec|
        spec.source_files = 'DuomLiveModule/Classes/ViewModel/**/*'
    end
    
    s.subspec 'Views' do |spec|
        spec.source_files = 'DuomLiveModule/Classes/Views/**/*'
    end
    
    
    s.dependency 'AgoraRtcEngine_iOS', '4.0.0-rc.1'
    s.dependency 'KKIMModule'
    s.dependency 'DuomBase'
    s.dependency 'DuomNetworkKit'
    
    
end
