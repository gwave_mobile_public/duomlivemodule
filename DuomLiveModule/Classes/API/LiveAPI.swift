//
//  LiveAPI.swift
//  Alamofire
//
//  Created by kuroky on 2022/10/12.
//

import Foundation
import DuomNetworkKit
import Moya

let liveProvider = MoyaProvider<LiveAPI>(plugins: NetworkConfig.plugins)

enum LiveAPI {
    // 修改直播名称
    case modifyTitle(eventId: String, title: String)
    // 开启直播
    case startLive(eventId: String)
    // 加入直播
    case joinLiveRoom(eventId: String)
    // 离开直播
    case quitLive(eventId: String)
    // 结束直播
    case endLive(eventId: String)
    // 查询活动详情
    case getLiveInfo(eventId: String, needEventOwner: Bool)
    
    // 获取礼物列表
    case getGiftList(artistId: String, limit: Int32, offset: Int32)
    // 发送礼物
    case sendGiftRequest(orderId: String)
    // 创建礼物订单
    case createOrder(giftId: String, channel: String, sendNow: Bool, roomId: String, scene: String)
}

extension LiveAPI: NetworkAPI {
    var parameters: [String : Any] {
        switch self {
        case .modifyTitle(let eventId, let title):
            return ["eventId": eventId, "title": title]
        case let .getLiveInfo(eventId: _, needEventOwner: needEventOwner):
            return ["needEventOwner": needEventOwner]
        case let .getGiftList(artistId: artistId, limit: limit, offset: offset):
            return ["artistId": artistId, "limit": limit, "offset": offset]
        case let .sendGiftRequest(orderId: orderId):
            return ["orderId": orderId]
        case let .createOrder(giftId: giftId, channel: channel, sendNow: sendNow, roomId: roomId, scene: scene):
            return ["giftId": giftId,
                    "channel": channel,
                    "sendNow": sendNow,
                    "roomId": roomId,
                    "scene": scene]
        default:
        return [:]
        }
    }
    
    var urlParameters: [String : Any] {
        return [:]
    }
    
    var method: Moya.Method {
        switch self {
        case .getLiveInfo, .getGiftList, .sendGiftRequest:
            return .get
        case .modifyTitle:
            return .put
        default:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .modifyTitle:
            return "/v1/celebrity/events"
        case let .startLive(eventId: eventId):
            return "/v1/celebrity/events/\(eventId)/start"
        case let .endLive(eventId: eventId):
            return "/v1/celebrity/events/\(eventId)/finish"
        case let .joinLiveRoom(eventId: eventId):
            return "/v1/celebrity/events/\(eventId)/join"
        case let .quitLive(eventId: eventId):
            return "/v1/celebrity/events/\(eventId)/leave"
        case let .getLiveInfo(eventId: eventId, needEventOwner: _):
            return "/v1/celebrity/events/\(eventId)"
        case .getGiftList(artistId: _, limit: _, offset: _):
            return "/v1/virtual/gifts"
        case .sendGiftRequest:
            return "/v1/virtual/gifts/send"
        case .createOrder:
            return "/v1/virtual/gifts/order"
        }
    }
    
}
