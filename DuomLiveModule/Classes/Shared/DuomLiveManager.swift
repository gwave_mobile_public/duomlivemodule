//
//  DuomLiveManager.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/10/14.
//

import Foundation
import DuomBase
import RxCocoa
import RxSwift
import DuomNetworkKit

public enum LiveMode {
    // 正在直播
    case livingMode
    // 预览模式
    case preMode
}

public class DuomLiveManager {
    private static let disposeBag = DisposeBag()
    // 跳转直播
    public static func openLiveRoom(eventId: String, role: LiveRole, liveMode: String? = nil, parentNavigator: UINavigationController?)  {
//        liveProvider.rx
//            .request(.getGiftList(artistId: <#T##String#>, limit: <#T##Int32#>, offset: <#T##Int32#>))
        
        
        if role == .Broadcaster {
            let anchorVC = LiveMainViewController().then {
                $0.viewModel = LiveViewModel(role: role,
                                             liveMode: liveMode == "1000" ? .livingMode : .preMode,
                                             userId: AccountManager.shared.currentAccount?.id ?? "",
                                             eventId: eventId)
            }
            parentNavigator?.pushViewController(anchorVC, animated: true)
        } else {
            let audienceVC = LiveAudienceMainViewController().then {
                $0.viewModel = LiveViewModel(role: role,
                                             liveMode: .livingMode,
                                             userId: AccountManager.shared.currentAccount?.id ?? "",
                                             eventId: eventId)
            }
            parentNavigator?.pushViewController(audienceVC, animated: true)
        }
    }
}
