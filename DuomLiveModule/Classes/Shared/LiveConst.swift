//
//  LiveConst.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/10/27.
//

import Foundation

// 点赞文案
let zanMessage = "Like the live"

// 送礼文案
let giftMessage = "sent a gift"

// 入场消息
let enterLiveMessage = "Joined"

// 主播关闭摄像头 观众端展示文案
let audienceWithHostCloseCameraText = "The host close the camera"

// 主播关闭摄像头 主播端展示文案
let hostCloseCameraText = "You have closed the camera"

// lottieSource资源读取
var getLiveLottieSource: (String, String) -> URL? = { sourceName, sourceExt in
    return Bundle(url: Bundle.main.url(forResource: "lottieSource", withExtension: "bundle")!)!.url(forResource: sourceName, withExtension: sourceExt)
}

// videoSource资源读取
var getVideoLottieSource: (String) -> String? = { sourceName in
    return Bundle(url: Bundle.main.url(forResource: "videoSource", withExtension: "bundle")!)!.path(forResource: sourceName, ofType: "mp4")
//    return Bundle(url: Bundle.main.url(forResource: "videoSource", withExtension: "bundle")!)!.url(forResource: sourceName, withExtension: sourceExt)
}

// 评论字数限制
let commentMaxCount: Int = 70

// 评论超出限制 提示文案
let commentOverTip = "Over word count"

let liveRoomStartNotification = Notification.Name("LIVEROOM_START_NOTIFICATION")

let liveRoomEndNotification = Notification.Name("LIVEROOM_END_NOTIFICATION")
