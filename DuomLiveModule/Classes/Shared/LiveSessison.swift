//
//  LiveSessison.swift
//  DuomChatRoom_Example
//
//  Created by kuroky on 2022/9/20.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import DuomBase
import KKIMModule

class LiveSessison: NSObject {
    typealias SuccessCompletion = ((String) -> Void)?
    typealias FailCompletion = ((Int32, String) -> Void)?
    typealias MemberListSuccessCompletion = ((Int, [LiveUserModel]) -> Void)?
    
    private var imHelper: DuomHelper = DuomHelper.sharedInstance()
    
    var role: LiveRole? = .Audience
    
    var userId: String = ""
    
    private var newmessage: BehaviorRelay<V2TIMMessage?> = BehaviorRelay<V2TIMMessage?>(value: nil)
    var newmessageObservable: Observable<V2TIMMessage?> {
        return newmessage.asObservable()
    }
    
    override init() {
        super.init()
    }
    
    convenience init(role: LiveRole?) {
        self.init()
        self.role = role
        imHelper.delegate = self
        addGroupMsgListener()
        addGroupListener()
    }
    
    func configSession() {
         imHelper.delegate = self
         addGroupMsgListener()
         addGroupListener()
    }
    
    deinit {
        Logger.log(message: "ChatSessison deinit", level: .info)
        removeGroupListener()
        removeGroupMsgListener()
    }
    
}

// MARK: functions
extension LiveSessison {
    func addGroupListener()  {
        imHelper.addGroupListener()
    }
    
    private func removeGroupListener()  {
        imHelper.removeGroupListener()
    }
    
    func addGroupMsgListener()  {
        imHelper.addAdvancedMsgListener()
    }
    
    private func removeGroupMsgListener()  {
        imHelper.removeAdvancedMsgListener()
    }
    
    func sendTextMessage(_ msg: String, groupID: String, successCompletion: SuccessCompletion, failCompletion: FailCompletion = nil)  {
        imHelper.sendMessage(msg, groupID: groupID, priority: DuomIMMessagePriority.DUOM_IM_PRIORITY_NORMAL) {
            Logger.log(message: "send text msg success", level: .info)
            if let success = successCompletion {
                success("")
            }
        } fail: { code, desc in
            Logger.log(message: "send text msg failure: \(desc)", level: .error)
            if let fail = failCompletion {
                fail(code, desc)
            }
        }
    }
    
    func sendCustomMessage(_ msg: String, groupID: String, successCompletion: SuccessCompletion, failCompletion: FailCompletion = nil)  {
        let data = msg.data(using: .utf8)!
        imHelper.sendGroupCustomMessage(data, groupID: groupID, priority: DuomIMMessagePriority.DUOM_IM_PRIORITY_NORMAL) {
            Logger.log(message: "send custom msg success", level: .info)
            if let success = successCompletion {
                success("")
            }
        } fail: { code, desc in
            Logger.log(message: "send custom msg failure: \(desc)", level: .error)
            if let fail = failCompletion {
                fail(code, desc)
            }
        }
    }
    
    func getGroupMemberList(_ groupID: String, successCompletion: MemberListSuccessCompletion, failComletion: FailCompletion = nil)  {
        imHelper.getGroupMemberList(groupID) { nextPage, list in
            
        } fail: { code, desc in
            if let failComletion = failComletion {
                failComletion(code, desc)
            }
        }
    }
    
    func joinGroup(_ groupId: String)  {
        imHelper.joinGroup(groupId) { [weak self] in
            guard let self = self else { return }
            print("join suc")
            self.addGroupListener()
            self.addGroupMsgListener()
        } fail: { code, desc in
            print("\(desc)")
        }
    }
    
    func leaveGroup(_ groupId: String)  {
        imHelper.quitGroup(groupId) { [weak self] in
            guard let self = self else { return }
            print("leave suc")
            self.removeGroupListener()
            self.removeGroupMsgListener()
        } fail: { code, desc in
            print("\(desc)")
            toast("leave group faiulre")
        }
    }
    
    func deinitAction()  {
        self.removeGroupMsgListener()
        self.removeGroupListener()
    }
    
}

extension LiveSessison: DuomIMDelegate {
    func onConnect(_ stateType: DuomIMStateType) {
        
    }
    
    func onRecvNewMessage(_ msg: V2TIMMessage) {
        
        newmessage.accept(msg)
    }
    
    func onMemberEnter(_ groupID: String, memberList: [V2TIMGroupMemberInfo]) {
     
    }
    
    func onMemberLeave(_ groupID: String, member: V2TIMGroupMemberInfo) {
        
    }
    
    func onReceiveRESTCustomData(_ groupID: String, data: Data) {
        
    }
    
    func onGroupDismissed(_ groupID: String, opUser: V2TIMGroupMemberInfo) {
        
    }
    
    func onMemberInfoChanged(_ groupID: String, changeInfoList: [V2TIMGroupMemberChangeInfo]) {
        
    }
    
    func onGroupInfoChanged(_ groupID: String, changeInfoList: [V2TIMGroupChangeInfo]) {
        
    }
    
    func onGrantAdministrator(_ groupID: String, opUser: V2TIMGroupMemberInfo, memberList: [V2TIMGroupMemberInfo]) {
        
    }
    
    func onRevokeAdministrator(_ groupID: String, opUser: V2TIMGroupMemberInfo, memberList: [V2TIMGroupMemberInfo]) {
        
    }
}
