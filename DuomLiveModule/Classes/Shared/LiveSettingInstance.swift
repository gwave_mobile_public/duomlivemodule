//
//  LiveSettingInstance.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/22.
//

import Foundation

class LiveSettingInstance {
    static let shared = LiveSettingInstance()
    
    var cameraIsFront: Bool = true
    var enableMirrod: Bool = false
    var liveRoomTile: String?
    
}
