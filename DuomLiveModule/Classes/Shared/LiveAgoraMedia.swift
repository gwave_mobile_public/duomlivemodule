//
//  LiveAgoraMedia.swift
//  DuomChatRoom_Example
//
//  Created by kuroky on 2022/9/20.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import AgoraRtcKit
import DuomBase
import UIKit

public extension AgoraRtcVideoCanvas {
    convenience init(streamId: UInt, view: UIView, mirrorMode: AgoraVideoMirrorMode = .disabled) {
        self.init()
        self.renderMode = .hidden
        self.uid = streamId
        self.mirrorMode = mirrorMode
        self.view = view
    }
}

public class LiveAgoraMedia: NSObject {
    public typealias JoinChannelCompletion = ((UInt) -> Void)?
    
    public var agoraKit: AgoraRtcEngineKit?
    
    public var role: LiveRole = .Audience
    
    public var channelStatus: PublishSubject<UInt> = PublishSubject<UInt>()
    
    public var snapStatus: PublishSubject<Bool> = PublishSubject<Bool>()
    
    public var hiddeRender: PublishSubject<Bool> = PublishSubject<Bool>()
    
    public var offlineState: PublishSubject<Void> = .init()
    
    public override init() {
        super.init()
    }
    
    public convenience init(role: LiveRole) {
        self.init()
        self.role = role
        agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId: AgoraAppId, delegate: self)
        agoraKit?.setChannelProfile(.liveBroadcasting)
        agoraKit?.setClientRole(role == .Broadcaster ? .broadcaster : .audience)
        agoraKit?.enableAudio()
        agoraKit?.enableVideo()
    }
    
    deinit {
         Logger.log(message: "LiveAgoraMedia deinit", level: .info)
    }
    
}

// MARK: functions
public extension LiveAgoraMedia {
    func startRenderLocalVideoStream(id: UInt, view: UIView, mirrorMode: AgoraVideoMirrorMode = .disabled) {
        Logger.log(message: "render local video stream: id--\(id), view frame:\(view.frame)", level: .info)
        let canvas = AgoraRtcVideoCanvas(streamId: id, view: view, mirrorMode: mirrorMode)
        agoraKit?.setupLocalVideo(canvas)
    }

    func startRenderRemoteVideoStream(id: UInt, view: UIView, mirrorMode: AgoraVideoMirrorMode = .disabled) {
        Logger.log(message: "start render remote video stream--> id: \(id), view frame: \(view.frame)", level: .info)
        let canvas = AgoraRtcVideoCanvas(streamId: id, view: view, mirrorMode: mirrorMode)
        agoraKit?.setupRemoteVideo(canvas)
    }
    
    func joinChannel(_ token: String, channelId: String, userID: String, completion: JoinChannelCompletion = nil)  {
        guard let agoraKit = agoraKit else { return }
        agoraKit.joinChannel(byToken: token, channelId: channelId, userAccount: userID) { s1, uid, s2 in
            Logger.log(message: "join channel success", level: .info)
            if let joinSuccess = completion {
                joinSuccess(uid)
            }
        }
    }
    
    func leaveChannel()  {
        guard let agoraKit = agoraKit else { return }
        agoraKit.leaveChannel()
    }
    
    func muteLocalAudioStream(_ mute: Bool)  {
        guard let agoraKit = agoraKit else { return }
        agoraKit.muteLocalAudioStream(mute)
    }

    func muteLocalVideoStream(_ mute: Bool) {
        guard let agoraKit = agoraKit else { return }
        agoraKit.muteLocalVideoStream(mute)
    }
    
    func enablAudioStram(_ enable: Bool) {
        guard let agoraKit = agoraKit else { return }
        agoraKit.enableLocalAudio(enable)
    }
    
    func enablVideoStram(_ enable: Bool) {
        guard let agoraKit = agoraKit else { return }
        agoraKit.enableLocalVideo(enable)
        agoraKit.muteLocalAudioStream(enable)
    }
    
    // 预览
    func startPreView()  {
        guard let agoraKit = agoraKit else { return }
        agoraKit.startPreview()
    }
    
    // 停止预览
    func stopPreView() {
        guard let agoraKit = agoraKit else { return }
        agoraKit.stopPreview()
    }
    
    // 切换摄像头
    func switchCamera()  {
        guard let agoraKit = agoraKit else { return }
        agoraKit.switchCamera()
    }
    
    // 镜像
//    func switSide() {
//        guard let agoraKit = agoraKit else { return }
//        agoraKit.
//    }
    
    // 截屏
    func takeSnapShot(_  uid: Int)  {
        guard let agoraKit = agoraKit else { return }
        let fileManager = FileManager.default
        if let filePath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first?.appending("/liveSnapShot.png") {
            if fileManager.fileExists(atPath: filePath) {
                do {
                    try fileManager.removeItem(atPath: filePath)
                    Logger.log(message: "remove image success: \(filePath)", level: .info)
                    fileManager.createFile(atPath: filePath, contents: nil, attributes: nil)
                } catch let removeError  {
                    Logger.log(message: "remove image error: \(filePath)", level: .error)
                }
            } else {
                fileManager.createFile(atPath: filePath, contents: nil, attributes: nil)
            }
            // save
            agoraKit.takeSnapshot(uid, filePath: filePath)
            self.snapStatus.onNext(true)
        }
    }
    
    func deinitAction()  {
        agoraKit?.leaveChannel()
        AgoraRtcEngineKit.destroy()
    }
    
}


extension LiveAgoraMedia: AgoraRtcEngineDelegate {
    public func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int) {
        Logger.log(message: "local join success: \(uid)", level: .info)
    }
    public func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        Logger.log(message: "remote join success: \(uid)", level: .info)
        self.channelStatus.onNext(uid)
    }
    
    public func rtcEngine(_ engine: AgoraRtcEngineKit, snapshotTaken uid: UInt, filePath: String, width: Int, height: Int, errCode: Int) {
        Logger.log(message: "filePath: \(filePath)  errcode: \(errCode)", level: .info)
        self.snapStatus.onNext(errCode == 0)
    }
    
    public func rtcEngine(_ engine: AgoraRtcEngineKit, remoteUserStateChangedOfUid uid: UInt, state: UInt) {
         Logger.log(message: "state change: \(state)", level: .info)
    }
    
    public func rtcEngine(_ engine: AgoraRtcEngineKit, remoteVideoStateChangedOfUid uid: UInt, state: AgoraVideoRemoteState, reason: AgoraVideoRemoteReason, elapsed: Int) {
         Logger.log(message: "remote video state: \(state),  reason: \(reason)", level: .info)
        if reason == .remoteMuted {
            self.hiddeRender.onNext(true)
        } else if reason == .remoteUnmuted {
            self.hiddeRender.onNext(false)
        }
    }
    
    public func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        Logger.log(message: "remote offline reason: \(reason.rawValue)", level: .info)
        self.offlineState.onNext(())
    }
    
}
