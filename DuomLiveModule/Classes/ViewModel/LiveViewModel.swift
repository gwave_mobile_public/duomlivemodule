//
//  LiveViewModel.swift
//  Pods
//
//  Created by kuroky on 2022/9/20.
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa
import AgoraRtcKit
import DuomNetworkKit
import KKIMModule

public class LiveViewModel: ViewModel {
    public enum Action: ResponderChainEvent {
        // 创建群组 加入频道 根据直播模式决定是否推流
        case startLive
        // 开始推流
        case publishStream
        // 主播结束直播
        case endLive
        // 观众进入直播
        case joinLiveRoom
        // 观众退出直播间
        case quitLive
        // 主播离开 观众退出频道
        case quitLiveChannel
        // 获取直播间信息
        case getLiveInfo
        // 截屏
        case snapShot
        // 发送text消息
        case sendTextMsg(msg: String)
        // 发送custom消息
        case sendCustomMsg(msg: String?, sticker: LiveChatStickerModel? = nil, gift: LiveGiftItem? = nil, type: LiveMessageType)
        case deinitAction
        // setting
        case switchFrontCamer
        case enableMic(enable: Bool)
        case enableVideo(enable: Bool)
        
        // 清除join/like历史消息
        case clearJoinedHistoryMsg
        // 清除礼物列表历史消息
        case clearGiftHistoryMsg
        
        // 发送礼物
        case sendGift(gift: LiveGiftItem)
    }
    
    public class State: Output {
        var beginPublishStream: PublishRelay<Void> = PublishRelay<Void>()
        
        var endLivePublish: PublishRelay<Void> = PublishRelay<Void>()
        
        var joinChannelStatus: PublishSubject<UInt> = PublishSubject<UInt>()
        
        var quitGroupStatus: PublishRelay<Void> = PublishRelay<Void>()
        
        var liveInfoPublish: PublishRelay<(String, String)> = PublishRelay<(String, String)>()
        
        var msgListData: PublishSubject<[Chat]> = .init()
        
        var joinMsgData: PublishSubject<[Chat]> = .init()
        
        var snapStatus: PublishSubject<Bool> = PublishSubject<Bool>()
        
        var hiddeRender: PublishSubject<Bool> = PublishSubject<Bool>()
        
        var offlineStatus: PublishSubject<Void> = .init()
        
        var anchorEndLive: PublishSubject<Void> = .init()
        
        var netErrorPublish: PublishSubject<Void> = .init()
        
        // 礼物列表
        var giftListRelay: BehaviorRelay<[LiveGiftItem]> = BehaviorRelay(value: [])
        
        // 待播放礼物列表 普通动效
        var giftPlayRelay: PublishRelay<[Chat]> = .init()
        // 播放礼物动效 大动效
        var bigGiftPlayRelay: BehaviorRelay<[LiveGiftItem]> = BehaviorRelay(value: [])
         
        required public init() {}
    }
    
    private(set) var chatList = [Chat]()
    
    private(set) var joinList = [Chat]()
    
    private(set) var giftPlayList = [Chat]()
    
    private let disposeBag = DisposeBag()
    
    var groupId = ""
    
    public var mediaKit: LiveAgoraMedia?
    
    var session: LiveSessison?
    
    var role: LiveRole
    
    var userId: String = AccountManager.shared.currentAccount?.id ?? ""
    
    var eventId: String = ""
    
    var liveMode: LiveMode = .livingMode
    
//    private(set) var pollCount: Int = 1
    
    public init(role: LiveRole, liveMode: LiveMode, userId: String, eventId: String) {
        self.role = role
        self.liveMode = liveMode
        self.userId = userId
        self.eventId = eventId
        self.mediaKit = LiveAgoraMedia(role: role)
        self.session = LiveSessison(role: role)
        
        bindMediaActions()
        bindSessionActions()
    }
    
    private func bindMediaActions() {
        self.mediaKit?.snapStatus.subscribe(onNext: { [weak self] success in
            self?.state.snapStatus.onNext(success)
        })
        .disposed(by: disposeBag)
        
        self.mediaKit?.channelStatus
            .bind(to: self.state.joinChannelStatus)
            .disposed(by: disposeBag)
        
        self.mediaKit?.hiddeRender
            .bind(to: self.state.hiddeRender)
            .disposed(by: disposeBag)
    }
    
    private func bindSessionActions()  {
        self.session?.newmessageObservable
            .subscribe(onNext: { [weak self] msg in
                guard let self = self, let msg = msg else { return }
                self.dealReciveMessage(msg)
            })
            .disposed(by: disposeBag)
    }
    
    public func mutate(action: Action, state: State) {
        switch action {
        case  .startLive:
            startLiveAction()
        case .publishStream:
            publishStreamAction()
        case .endLive:
            anchorEndLive()
        case .joinLiveRoom:
            joinLiveRoomAction()
        case .quitLive:
            quitLiveAction()
        case .quitLiveChannel:
            anchorLeaveChannel()
        case .snapShot:
            snapAction()
        case let .sendTextMsg(msg: msg):
            sendTextMessageAction(msg)
        case let .sendCustomMsg(msg: msg, sticker: sticker, gift: gift, type: type):
            sendCustomMessageAction(msg, sticker: sticker, gift: gift, type: type)
        case .deinitAction:
            deinitAction()
        case .switchFrontCamer:
            switchFrontCamera()
        case let .enableMic(enable):
            enableLocalMic(enable)
        case let .enableVideo(enable):
            enableLocalVideo(enable)
        case .getLiveInfo:
            getLiveRoomInfo()
        case .clearJoinedHistoryMsg:
            clearHistoryMsg()
        case .clearGiftHistoryMsg:
            clearGiftHistoryMsg()
        case let .sendGift(gift):
            sendGiftRequest(gift)
        }
    }
}

// MARK: Functions
extension LiveViewModel {
    private func getLiveRoomInfo()  {
        liveProvider.rx
            .request(.getLiveInfo(eventId: eventId, needEventOwner: true))
            .map(LiveRoomInfo.self)
            .do(onSuccess: { [weak self] info in
                guard let self = self, let celebrity = info.celebrity else { return }
                self.state.liveInfoPublish.accept((celebrity.nickName ?? "", celebrity.avatar ?? ""))
            })
            .asObservable()
                .flatMapLatest{ info -> Observable<LiveGiftModel> in
                    guard let celebrity = info.celebrity, let targetUid = celebrity.customerId else { return Observable.of(LiveGiftModel()) }
                    return liveProvider.rx
                        .request(.getGiftList(artistId: targetUid, limit: 50, offset: 0))
                        .map(LiveGiftModel.self)
                        .asObservable()
                }
                .subscribe(onNext: { [weak self] response in
                    guard let self = self else { return }
                    if let virtual = response.virtualGifts, virtual.rows.isNotEmpty {
                        self.state.giftListRelay.accept(virtual.rows)
                    }
                })
                .disposed(by: disposeBag)
    }
    
    // 明星开启直播
    private func startLiveAction()  {
        liveProvider.rx
            .request(.startLive(eventId: eventId))
            .map(LiveRoomJoinSuccessModel.self)
            .subscribe(onSuccess: { [weak self] succModel in
                guard let self = self else { return }
                if let groupId = succModel.groupId {
                    self.groupId = groupId
                }
                
                if let token = succModel.token,
                   let mediaKit = self.mediaKit,
                   let eventId = succModel.eventId {
                    mediaKit.leaveChannel()
                    mediaKit.joinChannel(token, channelId: eventId, userID: self.userId) { [weak self] uid in
                        self?.state.joinChannelStatus.onNext(uid)
                    }
                    self.session?.addGroupListener()
                    self.session?.addGroupMsgListener()
                    
                    mediaKit.muteLocalAudioStream(self.liveMode == .livingMode ? false : true)
                    mediaKit.muteLocalVideoStream(self.liveMode == .livingMode ? false : true)
                }
            }, onFailure: { [weak self] errorObj in
                print("\(errorObj.localizedDescription)")
                self?.state.netErrorPublish.onNext(())
            })
            .disposed(by: disposeBag)
    }
    
    
    // 主播开始推流
    private func publishStreamAction()  {
        guard let mediaKit = mediaKit else { return  }
        mediaKit.muteLocalAudioStream(false)
        mediaKit.muteLocalVideoStream(false)
        self.state.beginPublishStream.accept(())
    }
    
    // 主播结束直播
    private func anchorEndLive()  {
        liveProvider.rx
            .request(.endLive(eventId: eventId))
            .mapJSON()
            .showHUD()
            .subscribe(onSuccess: { [weak self] _ in
                guard let self = self, let mediaKit = self.mediaKit else { return }
                mediaKit.leaveChannel()
                // 发送结束通知
                NotificationCenter.default.post(name: Notification.Name("key_refresh_livestream"), object: nil)
                self.state.endLivePublish.accept(())
            }, onFailure: { [weak self] error in
                Logger.log(message: "end live error: \(error.localizedDescription)", level: .error)
            })
            .disposed(by: disposeBag)
    }
    
    private func joinLiveRoomAction()  {
        guard let mediaKit = mediaKit else { return }
        liveProvider.rx
            .request(.joinLiveRoom(eventId: eventId))
            .map(LiveRoomJoinSuccessModel.self)
            .subscribe(onSuccess: { [weak self] model in
                guard let self = self else { return }
                if let groupId = model.groupId,
                   let token = model.token,
                   let eventId = model.eventId {
                    self.groupId = groupId
                    mediaKit.leaveChannel()
                    mediaKit.joinChannel(token, channelId: eventId, userID: self.userId)
                    self.session?.joinGroup(self.groupId)
                    
                    self.sendCustomMessageAction(enterLiveMessage, type: .LIVE_JOIN)
                }
            }, onFailure: { [weak self] error in
                print("\(error.localizedDescription)")
                self?.state.netErrorPublish.onNext(())
            })
            .disposed(by: disposeBag)
    }
    
    // 观众退出直播间
    private func quitLiveAction()  {
        guard let mediaKit = mediaKit else { return }
        mediaKit.leaveChannel()
        liveProvider.rx
            .request(.quitLive(eventId: eventId))
            .mapJSON()
            .showHUD()
            .subscribe(onSuccess: { [weak self] succModel in
                guard let self = self else { return }
                self.session?.leaveGroup(self.groupId)
                self.state.quitGroupStatus.accept(())
            }, onFailure: { [weak self] error in
                print("\(error.localizedDescription)")
                self?.state.quitGroupStatus.accept(())
            })
            .disposed(by: disposeBag)
        
    }
    
    // 主播离开 观众退出频道
    private func anchorLeaveChannel()  {
        guard let mediaKit = mediaKit, let session = session else { return }
        mediaKit.leaveChannel()
        session.leaveGroup(self.groupId)
    }
    
    // 截屏
    private func snapAction()  {
        guard let mediaKit = mediaKit else { return }
        let fileManager = FileManager.default
        if let filePath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first?.appending("/liveSnapShot.jpg") {
            if !fileManager.fileExists(atPath: filePath) {
                do {
                    try fileManager.removeItem(atPath: filePath)
                    Logger.log(message: "remove image success: \(filePath)", level: .info)
                    fileManager.createFile(atPath: filePath, contents: nil, attributes: nil)
                } catch let err {
                    Logger.log(message: "remove image error: \(err)", level: .error)
                }
            } else {
                fileManager.createFile(atPath: filePath, contents: nil, attributes: nil)
            }
            // save
            mediaKit.agoraKit?.takeSnapshot(0, filePath: filePath)
        }
    }
    
    
    // send msg
    private func sendTextMessageAction(_ msg: String)  {
        guard let session = session else {
            toast("group empty")
            return
        }
        
        session.sendTextMessage(msg, groupID: groupId) { info in
            let chat: Chat = Chat(name: "\(AccountManager.shared.currentAccount?.nickName ?? "")", text: "\(msg)", image: AccountManager.shared.currentAccount?.avatar)
            self.chatList.insert(chat, at: 0)
            self.state.msgListData.onNext(self.chatList)
        }
    }
    
    // send custom msg
    // join/like 区别于其他消息
    private func sendCustomMessageAction(_ msg: String? = nil,
                                         sticker: LiveChatStickerModel? = nil,
                                         gift: LiveGiftItem? = nil,
                                         type: LiveMessageType)  {
        guard let session = session else {
            toast("group empty")
            return
        }
        //        "{\"msg\":{\"message\":\"2022101407082545802006\",\"cmd\":\"1002\"},\"customMsgType\":\"MSG_PUSH\",\"businessID\":\"custom_msg_push\"}"
        let msgDetailModel = LiveRoomMsgModel(message: msg, cmd: type.rawValue, sticker: sticker, gift: gift)
        let messageModel = LiveRoomMessageModel.init(msg: msgDetailModel, customMsgType: "MSG_PUSH", businessID: "custom_msg_push")
        let chat: Chat = Chat(name: "\(AccountManager.shared.currentAccount?.nickName ?? "")",
                              text: msg ?? "",
                              image: AccountManager.shared.currentAccount?.avatar,
                              gift: gift,
                              sticker: sticker)
        switch type {
        case .LIVE_JOIN, .ZAN_LIVE:
            self.joinList.insert(chat, at: 0)
            self.state.joinMsgData.onNext(self.joinList)
        case .LIVE_GIFT:
            if let gift = gift {
                if gift.showBigAnimation {
                    self.giftPlayList.insert(chat, at: 0)
                    self.state.giftPlayRelay.accept(self.giftPlayList)
                    self.state.bigGiftPlayRelay.accept([gift])
                } else {
                    self.giftPlayList.insert(chat, at: 0)
                    self.state.giftPlayRelay.accept(self.giftPlayList)
                }
            }
            
        default:
            self.chatList.insert(chat, at: 0)
            self.state.msgListData.onNext(self.chatList)
        }
        
        if let jsonString = messageModel.toJSONString() {
            session.sendCustomMessage(jsonString, groupID: groupId) { info in
                print("\(info)")
            }
        }
    }
    
    // 发送礼物接口
    private func sendGiftRequest(_ gift: LiveGiftItem)  {
        guard let giftId = gift.id, eventId.isNotEmpty else {
            toast("lose prama")
            return
        }
        liveProvider.rx
            .request(.createOrder(giftId: giftId,
                                  channel: gift.type == .gems ? "gems" : "applePay",
                                  sendNow: true,
                                  roomId: eventId,
                                  scene: "liveStream"))
            .map(LiveGiftOrder.self)
//            .showErrorHud()
//            .subscribe(onSuccess: { [weak self] order in
//                guard let orderId = order.orderId else { return }
//                self?.pollingToSendGift(gift, orderId)
//            }, onFailure: { error in
//                toast("failure to creat order")
//            })
            .flatMap({ order -> Single<LiveGiftSendReponse> in
                guard let orderId = order.orderId else { return Single.just(LiveGiftSendReponse()) }
                return liveProvider.rx
                    .request(.sendGiftRequest(orderId: orderId))
                    .map(LiveGiftSendReponse.self)
            })
            .subscribe(onSuccess: { [weak self] obj in
                guard let self = self else { return }
                self.sendCustomMessageAction(gift: gift, type: .LIVE_GIFT)
            }, onFailure: { error in
//                toast("\(error.localizedDescription)")
                toast("failure to creat order")
            })
            .disposed(by: disposeBag)
    }
    
    ///  轮询调接口 知道返回success  最多轮询5次
//    func pollingToSendGift(_ gift: LiveGiftItem, _ orderId: String)  {
//        guard pollCount <= 5 else { return }
//        liveProvider.rx
//            .request(.sendGiftRequest(orderId: orderId))
//            .map(LiveGiftSendReponse.self)
//            .subscribe(onSuccess: { [weak self] response in
//                if response.result == .success {
//                    self?.pollCount = 1
//                    self?.sendCustomMessageAction(gift: gift, type: .LIVE_GIFT)
//                } else {
//                    self?.pollCount += 1
//                    self?.pollingToSendGift(gift, orderId)
//                }
//            }, onFailure: { error in
//                toast(error.localizedDescription)
//            })
//            .disposed(by: disposeBag)
//    }
    
    private func deinitAction()  {
        guard let session = session, let media = mediaKit else { return }
        session.deinitAction()
        media.deinitAction()
    }
    
    // MARK: Gift
    private func getGiftAction(_ targetUid: String)  {
        liveProvider.rx
            .request(.getGiftList(artistId: targetUid, limit: 50, offset: 0))
            .subscribe(onSuccess: { obj in
                print("\(obj)")
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: Setting
    private func enableLocalMic(_ on: Bool)  {
        guard let mediaKit = mediaKit else { return }
        mediaKit.muteLocalAudioStream(!on)
    }
    
    private func enableLocalVideo(_ on: Bool)  {
        guard let mediaKit = mediaKit else { return }
        mediaKit.muteLocalVideoStream(!on)
        self.state.hiddeRender.onNext(!on)
    }
    
    private func switchFrontCamera()  {
        guard let mediaKit = mediaKit else { return }
        mediaKit.switchCamera()
    }
    
    // 处理消息
    private func dealReciveMessage(_ msg: V2TIMMessage)  {
        if msg.elemType == .ELEM_TYPE_TEXT {
            if let msgText = msg.textElem.text, !msgText.isEmpty {
                Logger.log(message: "msg is: \(msgText)", level: .info)
                let tempChat = Chat(name: msg.nickName ?? "", text: msgText, image: msg.faceURL)
                self.chatList.insert(tempChat, at: 0)
                self.state.msgListData.onNext(self.chatList)
            }
        } else if msg.elemType == .ELEM_TYPE_CUSTOM {
            if let custom = msg.customElem,
               let msgData = custom.data,
               let message = String(data: msgData, encoding: .utf8) {
                if let messageModel = LiveRoomMessageModel.deserialize(from: message),
                   let msgModel = messageModel.msg {
                    Logger.log(message: "\(msgModel)", level: .info)
                    if let msgCmd = msgModel.cmd {
                        switch msgCmd {
                        case LiveMessageType.DESTROY_GROUP.rawValue:
                            self.state.anchorEndLive.onNext(())
                        case LiveMessageType.ZAN_LIVE.rawValue:
                            if msg.sender != self.userId {
                                let tempChat = Chat(name: msg.nickName ?? "", text: zanMessage, image: msg.faceURL)
                                self.joinList.insert(tempChat, at: 0)
                                self.state.joinMsgData.onNext(self.joinList)
                            }
                        case LiveMessageType.LIVE_JOIN.rawValue:
                            if msg.sender != self.userId {
                                let tempChat = Chat(name: msg.nickName ?? "", text: enterLiveMessage, image: msg.faceURL)
                                self.joinList.insert(tempChat, at: 0)
                                self.state.joinMsgData.onNext(self.joinList)
                            }
                        case LiveMessageType.LIVE_GIFT.rawValue:
                            if msg.sender != self.userId, let gift = msgModel.gift {
                                let chat: Chat = Chat(name: msg.nickName ?? "", text: nil, image: msg.faceURL, gift: gift)
                                if gift.showBigAnimation {
                                    self.giftPlayList.insert(chat, at: 0)
                                    self.state.giftPlayRelay.accept(self.giftPlayList)
                                    self.state.bigGiftPlayRelay.accept([gift])
                                } else {
                                    self.giftPlayList.insert(chat, at: 0)
                                    self.state.giftPlayRelay.accept(self.giftPlayList)
                                }
                            }
                        case LiveMessageType.LIVE_STICKER.rawValue:
                            if msg.sender != self.userId, let sticker = msgModel.sticker {
                                let chat: Chat = Chat(name: msg.nickName ?? "",
                                                      text: nil,
                                                      image: msg.faceURL,
                                                      gift: nil,
                                                      sticker: sticker)
                                self.chatList.insert(chat, at: 0)
                                self.state.msgListData.onNext(self.chatList)
                            }
                        default:
                            break
                        }
                    }
                } else {
                    if msg.sender == "administrator" {
                        return
                    }
                    Logger.log(message: "msg is: \(message)", level: .info)
                }
            }
        }
    }
    
    // MARK: 清楚join/like历史消息
    private func clearHistoryMsg()  {
        self.joinList.removeAll()
    }
    
    private func clearGiftHistoryMsg()  {
        self.giftPlayList.removeAll()
    }
}
