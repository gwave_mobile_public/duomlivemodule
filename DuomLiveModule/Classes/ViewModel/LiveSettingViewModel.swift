//
//  LiveSettingViewModel.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/21.
//

import Foundation
import RxCocoa
import RxSwift
import DuomBase
import AgoraRtcKit
import DuomNetworkKit

class LiveSettingViewModel: ViewModel {
    enum Action: ResponderChainEvent {
        case saveTitleSetting(title: String)
        case saveMediaSetting
        case switchFrontCamer
        case switchSide(isLeft: Bool)
    }
    
    class State: Output {
        var setTitleStatus: PublishRelay<Void> = PublishRelay<Void>()
        
        var switchStatus: PublishSubject<Bool> = PublishSubject<Bool>()
        
        var leftSideStatus: PublishSubject<Bool> = PublishSubject<Bool>()
        
        required public init() {}
    }
    
    var mediaKit: LiveAgoraMedia?
    
    var eventId: String = ""
    
    private let disposeBag = DisposeBag()
    
    func mutate(action: Action, state: State) {
        switch action {
        case let .saveTitleSetting(title: title):
            saveSetting(title)
        case .saveMediaSetting:
            saveMediaSettingAction()
        case .switchFrontCamer:
            switchFrontCamera()
        case let .switchSide(isLeft):
            switchSide(isLeft)
        }
    }
    
}

extension LiveSettingViewModel {
    private func saveSetting(_ title: String)  {
        liveProvider.rx
            .request(.modifyTitle(eventId: eventId, title: title))
            .mapJSON()
            .showHUD(.mask)
            .subscribe(onSuccess: { [weak self] json in
                guard let self = self else { return }
                print("\(json)")
            }, onError: { error in
                toast("\(error.localizedDescription)")
            })
            .disposed(by: disposeBag)
    }
    
    private func saveMediaSettingAction()  {
        
    }
    
    private func switchFrontCamera()  {
        guard let mediaKit = mediaKit else { return }
        mediaKit.switchCamera()
    }
    
    private func switchSide(_ isLeftSide: Bool) {
        mediaKit?.agoraKit?.setLocalRenderMode(.hidden, mirror: isLeftSide ? .disabled : .enabled)
    }
}
