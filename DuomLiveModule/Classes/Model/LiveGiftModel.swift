//
//  LiveGiftModel.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/11/9.
//

import Foundation
import RxDataSources
import DuomNetworkKit
import HandyJSON

struct LiveGiftModel: JSONCodable {
    var gems: Double = 0.0
    var virtualGifts: LiveGiftsList?
}

struct LiveGiftsList: JSONCodable {
    var totalCount: Int64?
    var limit: Int32?
    var offset: Int32?
    var rows: [LiveGiftItem] = []
}

public struct LiveGiftItem: JSONCodable {
    var animationTime: Int32?
    var applePayUrl: String?
    var level: GiftLevel = .primary
    var icon: String?
    var artistId: String?
    var sort: Int32?
    var type: GiftType?
    var stripeParams: String?
    var animation: Int32?
    var stripeUrl: String?
    var applePayParams: String?
    var price: Double?
    var name: String?
    var startTime: Int64?
    var id: String?
    var endTime: Int64?
    var animationMaterial: String?
    
    /*
     animation = 0
     animationMaterial != nil
     */
    var showBigAnimation: Bool {
        return (animation == 0 && animationMaterial != nil)
    }
    
    public init() {}
}

enum GiftLevel: String, JSONCodableEnum {
    case primary = "primary"
    case middle = "middle"
    case high = "high"
}

enum GiftType: String, JSONCodableEnum {
    case gems = "gems"
    case currency = "currency"
}

struct LiveGiftSection {
    var items: [LiveGiftItem]
}

extension LiveGiftSection: SectionModelType {
    init(original: LiveGiftSection, items: [LiveGiftItem]) {
        self = original
        self.items = items
    }
}


// 创建礼物接口回调
public struct LiveGiftOrder: JSONCodable {
    var giftId: String?
    var orderId: String?
    var orderStatus: String?
    
    public init() {}
}

// 发送礼物接口回调
public struct LiveGiftSendReponse: JSONCodable {
    var result: GiftReponseType = .wait
    
    public init() {}
}

enum GiftReponseType: String, JSONCodableEnum {
    case wait = "wait"
    case success = "success"
}
