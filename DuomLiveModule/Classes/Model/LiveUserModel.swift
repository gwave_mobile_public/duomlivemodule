//
//  LiveUserModel.swift
//  Pods
//
//  Created by kuroky on 2022/9/20.
//

import Foundation
import RxDataSources
import DuomNetworkKit
import HandyJSON
import DuomBase

public enum LiveRole {
    case Broadcaster
    case Audience
}

public struct LiveUserModel {
    public var role: LiveRole?
    public var isMuted: Bool = false
    public var isSelfMuted: Bool = false
    public var headerImg: String?
    public var name: String?
    public var uId: String?
}

struct Chat {
    var name: String?
//    var textSize: CGSize
    var content: NSAttributedString
    var image: String?
    
    var gift: LiveGiftItem?
    
    var sticker: LiveChatStickerModel?
    
    init(name: String?, text: String?, image: String? = nil, gift: LiveGiftItem? = nil, sticker: LiveChatStickerModel? = nil) {
//        let content = text as NSString
//        let width = UIScreen.main.bounds.width - 60
//        let textRect = content.boundingRect(with: CGSize(width: width, height: CGFloat(MAXFLOAT)),
//                                            options: .usesLineFragmentOrigin,
//                                            attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: .medium)],
//                                            context: nil)
//
//        let attrContent = NSMutableAttributedString(string: (content as String))
//
//        attrContent.addAttributes([.foregroundColor: UIColor.white,
//                                   .font: UIFont.systemFont(ofSize: 14, weight: .medium)],
//                                  range: NSRange(location: 0, length: tName.count))
//
//        attrContent.addAttributes([.foregroundColor: UIColor.white,
//                                   .font: UIFont.systemFont(ofSize: 14)],
//                                  range: NSRange(location: tName.count, length: text.count))
//
//        var adjustSize = textRect.size
//        adjustSize.width = adjustSize.width + 2
//        self.name = name
//        self.textSize = adjustSize
//        self.content = attrContent
//        self.image = image
        
        self.name = name
        self.content = NSMutableAttributedString(string: text ?? "").addAttributes {
            $0.textColor(.textWhiteColor).font(.appFont(ofSize: 13, fontType: .Inter_Medium))
        }
        self.image = image
        
        self.gift = gift
        self.sticker = sticker
    }
}


// live chat
struct LiveChatSection {
    var items: [Chat]
}

extension LiveChatSection: SectionModelType {
    init(original: LiveChatSection, items: [Chat]) {
        self = original
        self.items = items
    }
}


// live set
enum LiveSetModel {
    case switchCell(isSelected: Bool)
    case tipCell
    case sideCell(isLeft: Bool)
}

struct LiveSetSection {
    var items: [LiveSetModel]
}

extension LiveSetSection: SectionModelType {
    init(original: LiveSetSection, items: [LiveSetModel]) {
        self = original
        self.items = items
    }
}



// MARK: joinRoom
public struct LiveRoomJoinSuccessModel: JSONCodable {
    var eventId: String?
    var groupId: String?
    var token: String?
    
    public init() {}
}

// MARK: liveInfo
struct LiveRoomInfo: JSONCodable {
    var celebrity: LiveRoomCelebrityModel?
    var estimatedStart: String?
    var owner: String?
    var role: String?
    var surface: String?
    var created: String?
    var estimatedEnd: String?
    var timeZone: String?
    var description: String?
    var subscribeStatus: String?
    var title: String?
    var type: String?
    var topic: String?
    var id: String?
    var followerCount: String?
    var status: String?
    var followers: [LiveRoomFollwerModel] = [LiveRoomFollwerModel]()
}

struct LiveRoomFollwerModel: JSONCodable {
    var customerId: String?
    var avatar: String?
}

struct LiveRoomCelebrityModel: JSONCodable {
    var avatar: String?
    var customerId: String?
    var gender: String?
    var intro: String?
    var nickName: String?
}


// MARK: 消息类型
public enum LiveMessageType: String {
    case CHANGE_GROUP_NAME = "1009"
    case DESTROY_GROUP = "1010"
    case LEVE_GROUP = "1011"
    case ZAN_LIVE = "2011"
    case LIVE_GIFT = "2012"
    case LIVE_JOIN = "2013"
    case LIVE_STICKER = "2014"
}

public struct LiveRoomMessageModel: JSONCodable {
    var msg: LiveRoomMsgModel?
    var customMsgType: String?
    var businessID: String?
    
    public init() {}
    
    public init(msg: LiveRoomMsgModel, customMsgType: String? = nil, businessID: String? = nil) {
        self.msg = msg
        self.customMsgType = customMsgType
        self.businessID = businessID
    }
}

public struct LiveRoomMsgModel: JSONCodable {
    var message: String?
    var cmd: String?
    var token: String?
    var sticker: LiveChatStickerModel?
    var gift: LiveGiftItem?
    
    
    public init() {}
    
    public init(message: String?, cmd: String?, token: String? = nil, sticker: LiveChatStickerModel? = nil, gift: LiveGiftItem? = nil) {
        self.message = message
        self.cmd = cmd
        self.token = token
        self.sticker = sticker
        self.gift = gift
    }
}

public struct LiveChatStickerModel: JSONCodable {
    var decryptOriginal: String?
    
    public init() {}
}

