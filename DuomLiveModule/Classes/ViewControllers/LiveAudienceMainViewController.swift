//
//  LiveAudienceMainViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/23.
//  用户端直播主页面
//

import Foundation
import DuomBase
import UIKit
import RxSwift
import RxCocoa
import RxViewController
import AgoraRtcKit

public class LiveAudienceMainViewController: BaseViewController, ViewType, ResponderChainRouter, LiveRoomSubModule {
    public typealias Event = LiveViewModel.Action
    public typealias TargetViewModel = LiveViewModel
    
    public let renderView = UIView(backgroundColor: .clear)
    
    private let placeHolderView = LiveConnectingView().then {
        $0.tip = audienceWithHostCloseCameraText
    }
    
    private lazy var containerVC = LiveAudienceContainerViewController()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        subModuleService.inject(viewModel)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // 开启防截屏录制开关
        NotificationCenter.default.post(Notification(name: liveRoomStartNotification))
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // 关闭防截屏录制开关
        NotificationCenter.default.post(Notification(name: liveRoomEndNotification))
    }
    
    public override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = UIColor.themeBlackColor
        view.addSubview(renderView)

        addChild(containerVC)
        view.addSubview(containerVC.view)
        containerVC.didMove(toParent: self)

        view.sendSubviewToBack(renderView)

        renderView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        containerVC.view.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalToSuperview()
        }

        LiveHUD.showLiveHud(from: self.renderView)
    }

    public func bind(viewModel: LiveViewModel) {

        rx.viewWillAppear
            .map { _ in .joinLiveRoom }
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)

        rx.viewWillDisappear
            .map {_ in .deinitAction}
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)

        viewModel.state.netErrorPublish
            .subscribe(onNext: { [weak self] in
                delay(2.0) { [weak self] in
                    self?.removeSubViewControllers()
                    LiveHUD.hiddenLiveHud()
//                    self?.navigationController?.popToRootViewController(animated: true)
                }
            })
            .disposed(by: disposeBag)

        viewModel.state.joinChannelStatus
            .subscribe(onNext: { [weak self] uid in
                LiveHUD.hiddenLiveHud()
                self?.startRemoteRenderView(uid)
            })
            .disposed(by: disposeBag)

        viewModel.state.quitGroupStatus
            .subscribe(onNext: { [weak self] in
                self?.navigationController?.popToRootViewController(animated: true)
            })
            .disposed(by: disposeBag)

        viewModel.state.hiddeRender
            .subscribe(onNext: { [weak self] hidden in
                self?.setPlaceHolderViewStatus(hidden)
            })
            .disposed(by: disposeBag)

        viewModel.state.anchorEndLive
            .subscribe(onNext: { [weak self] in
                self?.showBrocasterLiveAlert()
            })
            .disposed(by: disposeBag)

        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
}

// MARK: Agora funs
extension LiveAudienceMainViewController {
    public func startRemoteRenderView(_ uid: UInt)  {
        guard let mediaKit = self.mediaKit else { return }
        mediaKit.startRenderRemoteVideoStream(id: uid, view: self.renderView)
    }

    private func showBrocasterLiveAlert() {
        let alert = LiveRoomAlert(title: "End of livestream", message: "The host has ended the livestream. Do you want to leave this livestream?", cancelTitle: "YES", fixedHeight: 60.DScale)
        alert.cancelAction = { [weak self] _ in
            self?.viewModel?.action.onNext(.quitLiveChannel)
            self?.navigationController?.popViewController(animated: true)
        }
        self.present(alert, animated: true)
    }

    // 主播关闭了推流
    private func setPlaceHolderViewStatus(_ show: Bool)  {
        if show {
            renderView.addSubview(placeHolderView)
            placeHolderView.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
        } else {
            placeHolderView.removeFromSuperview()
        }
    }

}

extension LiveAudienceMainViewController {
    // 退出前移除所有模态出的视图
    private func removeSubViewControllers()  {
        if let currentNav = self.navigationController {
            if var navPresentVC = currentNav.presentedViewController {
                while navPresentVC.presentedViewController != nil {
                    navPresentVC = navPresentVC.presentedViewController!
                }
                navPresentVC.dismiss(animated: false)
            }
        }
    }
}
