//
//  LiveSetTitleViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/21.
//

import Foundation
import DuomBase
import RxCocoa
import RxSwift
import RxGesture
import UIKit

class LiveSetTitleViewController: PresentationViewController, ViewType, ResponderChainRouter {
    typealias TargetViewModel = LiveSettingViewModel
    typealias Event = LiveSettingViewModel.Action
    
    private let topView = UIView().then {
        $0.backgroundColor = .clear
    }
    
    private let containerView = UIView().then {
        $0.backgroundColor = UIColor.themeBlackColor
        $0.roundCorners(radius: 10)
    }
    
    private let headerImageView = RoundImageView().then {
        $0.image = UIImage(named: "live_user_placeholder")
    }
    
    private let textField = UITextField().then {
        $0.textAlignment = .left
        $0.placeholder = "Add a title..."
        $0.font = .appFont(ofSize: 16, fontType: .Inter_Medium)
        $0.textColor = UIColor.textWhiteColor
        $0.returnKeyType = .done
    }
    
    private let lineView = UIView().then {
        $0.backgroundColor = UIColor.themeLineColor
    }
    
    private let tipLabel = UILabel().then {
        $0.numberOfLines = 0
        $0.textColor = UIColor.textWhiteColor
        $0.font = .appFont(ofSize: 14, fontType: .Inter_Medium)
        $0.textAlignment = .left
        $0.text = "Your followers and anyone watching will see this title."
    }
    
    private let addButton = DuomThemeButton(style: .highLight, title: "ADD TITLE")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardObserve(aNotification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardObserve(aNotification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        addViewActions()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        textField.delegate = self
        
        if let img = AccountManager.shared.currentAccount?.avatar, !img.isEmpty {
            headerImageView.kf.setImage(with: URL(string: img))
        }
        
        view.backgroundColor = .clear
        view.addSubview(topView)
        view.addSubview(containerView)
        containerView.addSubview(headerImageView)
        containerView.addSubview(textField)
        containerView.addSubview(lineView)
        containerView.addSubview(tipLabel)
        containerView.addSubview(addButton)
        
        topView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(556.DScale.toCeil)
        }
        
        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.width.equalTo(UIScreen.screenWidth)
            make.top.equalTo(topView.snp.bottom)
            make.bottom.equalToSuperview()
            make.height.equalTo(UIScreen.screenHeight - 556.DScale)
        }
        
        headerImageView.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.top.equalTo(24.DScale)
            make.size.equalTo(CGSize(width: 50, height: 50).DScale)
        }
        
        textField.snp.makeConstraints { make in
            make.leading.equalTo(headerImageView.snp.trailing).offset(16)
            make.trailing.equalTo(-25)
            make.height.equalTo(40)
            make.centerY.equalTo(headerImageView.snp.centerY)
        }
        
        lineView.snp.makeConstraints { make in
            make.top.equalTo(headerImageView.snp.bottom).offset(16.DScale)
            make.height.equalTo(1.DScale)
            make.leading.equalTo(headerImageView.snp.leading)
            make.trailing.equalTo(-16)
        }
        
        tipLabel.snp.makeConstraints { make in
            make.top.equalTo(lineView.snp.bottom).offset(24.DScale)
            make.leading.equalTo(headerImageView.snp.leading)
            make.trailing.equalTo(lineView.snp.trailing)
        }
        
        addButton.snp.makeConstraints { make in
            make.leading.equalTo(54)
            make.trailing.equalTo(-54)
            make.top.equalTo(tipLabel.snp.bottom).offset(36.DScale)
            make.height.equalTo(36.DScale)
        }
    }
    
    private func addViewActions()  {
        topView.rx.tapGesture()
            .when(.recognized)
            .subscribe(onNext: { [weak self] _ in
                self?.dismiss(animated: true)
            })
            .disposed(by: disposeBag)
        
        addButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                if let title = self.textField.text,
                    title.isNotEmpty {
                    self.viewModel?.action.onNext(.saveTitleSetting(title: title))
                }
                self.dismiss(animated: true)
            })
            .disposed(by: disposeBag)
        
        viewModel?.state.setTitleStatus
            .subscribe(onNext: { [weak self] in
                self?.dismiss(animated: true)
            })
            .disposed(by: disposeBag)
    }
    
}

extension LiveSetTitleViewController {
    func bind(viewModel: LiveSettingViewModel) {
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
}

extension LiveSetTitleViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: Keyboard
extension LiveSetTitleViewController {
    @objc private func keyboardObserve(aNotification: NSNotification)  {
        let dict = aNotification.userInfo as! [String: Any]
        let value = dict[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let duration = (dict[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let rect = value.cgRectValue
        if aNotification.name == UIResponder.keyboardWillShowNotification {
            topView.snp.updateConstraints { make in
                make.height.equalTo(556.DScale.toCeil - rect.size.height)
            }
            containerView.snp.updateConstraints { make in
                make.height.equalTo(UIScreen.screenHeight - 556.DScale + rect.size.height)
            }
            
            UIView.animate(withDuration: duration) {
                self.view.layoutIfNeeded()
                self.topView.layoutIfNeeded()
                self.containerView.layoutIfNeeded()
            }
        }
        if aNotification.name == UIResponder.keyboardWillHideNotification {
            topView.snp.updateConstraints { make in
                make.height.equalTo(556.DScale.toCeil)
            }
            containerView.snp.updateConstraints { make in
                make.height.equalTo(UIScreen.screenHeight - 556.DScale)
            }
            
            UIView.animate(withDuration: duration, animations: {
                self.view.layoutIfNeeded()
                self.topView.layoutIfNeeded()
                self.containerView.layoutIfNeeded()
            })
        }
    }
}
