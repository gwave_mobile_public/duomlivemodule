//
//  LiveSetCameraViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/21.
//

import Foundation
import RxDataSources
import RxSwift
import RxCocoa
import DuomBase
import SwiftUI

class LiveSetCameraViewController: PresentationViewController, ViewType, ResponderChainRouter {
    typealias TargetViewModel = LiveSettingViewModel
    typealias Event = LiveSettingViewModel.Action
    
    private let tableView = UITableView().then {
        $0.backgroundColor = .themeBlackColor
        $0.showsVerticalScrollIndicator = false
        $0.showsHorizontalScrollIndicator = false
        $0.isScrollEnabled = false
        $0.rowHeight = UITableView.automaticDimension
        $0.register(LiveSetCameraSwitchCell.self, forCellReuseIdentifier: LiveSetCameraSwitchCell.identifier)
        $0.register(LiveSetCameraTipCell.self, forCellReuseIdentifier: LiveSetCameraTipCell.identifier)
        $0.register(LiveSetCameraSideCell.self, forCellReuseIdentifier: LiveSetCameraSideCell.identifier)
    }
    
    private let titleLabel = UILabel().then {
        $0.textColor = .textWhiteColor
        $0.textAlignment = .center
        $0.font = .appFont(ofSize: 16, fontType: .Inter_Blod)
        $0.text = "Controls"
    }
    
    private let headerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.screenWidth, height: 55)).then {
        $0.backgroundColor = .clear
    }
    
    private let footerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.screenWidth, height: 102)).then {
        $0.backgroundColor = .clear
    }
    
    private let doneButton = DuomThemeButton(style: .highLight, title: "DONE")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addActions()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        view.backgroundColor = .themeBlackColor
        view.roundCorners(radius: 10)
        view.addSubview(tableView)
        headerView.addSubview(titleLabel)
        footerView.addSubview(doneButton)
        
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.width.equalTo(UIScreen.screenWidth)
            make.height.equalTo(365.DScale)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(24)
            make.bottom.equalTo(-12)
            make.height.equalTo(19)
        }
        
        doneButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.equalTo(268.DScale)
            make.height.equalTo(36)
            make.top.equalTo(25)
        }
        
        doneButton.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.dismiss(animated: true)
            })
            .disposed(by: disposeBag)
        
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerView
    }
    
    private func addActions()  {
         let items = Observable.just([
            LiveSetSection.init(items: [
                LiveSetModel.switchCell(isSelected: LiveSettingInstance.shared.cameraIsFront),
                LiveSetModel.tipCell,
                LiveSetModel.sideCell(isLeft: !LiveSettingInstance.shared.enableMirrod)
            ])
        ])
        
        let dataSource = RxTableViewSectionedReloadDataSource<LiveSetSection> (configureCell: {
            (datasource, tableview, indexPath, element) in
            switch element {
            case let .switchCell(isSelected):
                let cell = tableview.dequeueReusableCell(withIdentifier: LiveSetCameraSwitchCell.identifier, for: indexPath) as! LiveSetCameraSwitchCell
                cell.configCell(isSelected: isSelected)
                return cell
            case .tipCell:
                let cell = tableview.dequeueReusableCell(withIdentifier: LiveSetCameraTipCell.identifier, for: indexPath) as! LiveSetCameraTipCell
                return cell
            case let .sideCell(isLeft):
                let cell = tableview.dequeueReusableCell(withIdentifier: LiveSetCameraSideCell.identifier, for: indexPath) as! LiveSetCameraSideCell
                cell.configCell(isLeft: isLeft)
                return cell
            }
        })
        
        items.bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
    
    func bind(viewModel: LiveSettingViewModel) {
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
    
}
