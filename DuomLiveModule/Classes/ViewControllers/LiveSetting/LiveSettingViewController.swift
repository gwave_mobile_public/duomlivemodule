//
//  LiveSettingViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/21.
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa
import RxViewController

class LiveSettingViewController: LiveSubModuleViewController {
    typealias Event = LiveViewModel.Action
    
    private let startButton = UIButton().then {
        $0.backgroundColor = UIColor.themeColor
        $0.setImage(UIImage(named: "live_start_icon"), for: .normal)
    }
    
    private let settingStackView = UIView()
    
    private let titleButton = DuomButton().then {
        $0.duomAlignment = .default
        $0.padding = 8
        $0.setEnlargeEdge(15)
        $0.setImage(UIImage(named: "live_title_icon"), for: .normal)
        $0.setTitle("Title", for: .normal)
        $0.titleLabel?.font = .appFont(ofSize: 12, fontType: .Inter_Medium)
    }
    
    private let otherButton = DuomButton().then {
        $0.duomAlignment = .default
        $0.padding = 8
        $0.setEnlargeEdge(15)
        $0.setImage(UIImage(named: "live_set_other__icon"), for: .normal)
        $0.setTitle("Other Settings", for: .normal)
        $0.titleLabel?.font = .appFont(ofSize: 12, fontType: .Inter_Medium)
    }
    
    private lazy var titleVC: LiveSetTitleViewController = LiveSetTitleViewController()
    
    lazy var settingViewModel: LiveSettingViewModel = LiveSettingViewModel().then {
        $0.mediaKit = self.viewModel?.mediaKit
        $0.eventId = self.viewModel?.eventId ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addViewsAction()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = .clear
        view.addSubview(startButton)
        view.addSubview(settingStackView)
        
        startButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(-30)
            make.centerX.equalToSuperview()
            make.size.equalTo(CGSize(width: 49, height: 49).DScale)
        }
        startButton.roundCorners(radius: 49.DScale / 2)
        
        settingStackView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(14)
            make.width.greaterThanOrEqualTo(100.DScale)
            make.height.greaterThanOrEqualTo(60.DScale)
        }
        settingStackView.vstack(titleButton, otherButton, spacing: 26, alignment: .leading, distribution: .fillEqually)
    }
    
    private func addViewsAction()  {
        startButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.willMove(toParent: nil)
                self?.view.removeFromSuperview()
                self?.removeFromParent()
                self?.viewModel?.action.onNext(.publishStream)
            })
            .disposed(by: disposeBag)
        
        titleButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.present(self.titleVC, animated: true)
            })
            .disposed(by: disposeBag)
        
        otherButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                let setVC = LiveSetCameraViewController().then {
                    $0.viewModel = self?.settingViewModel
                }
                self?.present(setVC, animated: true)
            })
            .disposed(by: disposeBag)
    }
    
    override func bind(viewModel: LiveViewModel) {
        titleVC.viewModel = settingViewModel
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
    
}
