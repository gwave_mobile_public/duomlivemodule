//
//  LiveStickerLayout.swift
//  DuomLiveModule
//
//  Created by kuroky on 2023/2/16.
//

import Foundation

class LiveStickerLayout: UICollectionViewLayout {
    var line = 2
    var column = 3
    private var layoutAttributes = [UICollectionViewLayoutAttributes]()

    private var sectionIntegratingPageCount: [Int] = []

    override func prepare() {
        super.prepare()
        guard let collectionView = collectionView else {
            return
        }

        sectionIntegratingPageCount = []
        layoutAttributes = []
        let sectionCount = collectionView.numberOfSections
        var page: CGFloat = 0
        for section in 0 ..< sectionCount {
            let itemNum = collectionView.numberOfItems(inSection: section)
            if itemNum > 0 {
                for index in 0 ..< itemNum {
                    let indexPath = IndexPath(row: index, section: section)
                    let attributes = layoutAttributesForItem(at: indexPath, by: Int(page))
                    layoutAttributes.append(attributes)
                }
            } else {
                if let headerLayout = layoutAttributesForSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, at: IndexPath(row: 0, section: section)) {
                    layoutAttributes.append(headerLayout)
                }
            }

            var sectionPage = ceil(CGFloat(itemNum) / CGFloat(line * column))
            sectionPage = max(sectionPage, 1)
            page += sectionPage
            sectionIntegratingPageCount.append(Int(page))
        }
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return layoutAttributes.filter { rect.contains($0.frame) || rect.intersection($0.frame).size != .zero }
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return layoutAttributes.first(where: { $0.indexPath == indexPath })
    }

    override var collectionViewContentSize: CGSize {
        guard let collectionView = collectionView else {
            return .zero
        }
        return CGSize(width: CGFloat(sectionIntegratingPageCount.last ?? 0) * collectionView.frame.width, height: collectionView.frame.height)
    }

    private func layoutAttributesForItem(at indexPath: IndexPath, by startPage: Int) -> UICollectionViewLayoutAttributes {
        let collectionView = self.collectionView!

        let page = indexPath.row / (line * column)
        let pageOffset = CGFloat(page) * collectionView.frame.width + CGFloat(startPage) * collectionView.frame.width

        let i = indexPath.row % (line * column)
        let containerSize = collectionView.frame.size
        let w = containerSize.width / CGFloat(column)
        let h = containerSize.height / CGFloat(line)
        let x = CGFloat(i % column) * w
        let y = CGFloat(i / column) * h

        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        attributes.frame = CGRect(x: floor(x + pageOffset),
                                  y: floor(y),
                                  width: ceil(w),
                                  height: ceil(h))
        return attributes
    }

    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let collectionView = collectionView, collectionView.numberOfItems(inSection: indexPath.section) == 0 else {
            return nil
        }

        let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: elementKind, with: indexPath)
        attributes.frame = collectionView.bounds.with {
            let pageCount = indexPath.section > 0 ? sectionIntegratingPageCount[indexPath.section - 1] : 0
            $0.origin.x = collectionView.frame.size.width * CGFloat(pageCount)
        }
        return attributes
    }
}
