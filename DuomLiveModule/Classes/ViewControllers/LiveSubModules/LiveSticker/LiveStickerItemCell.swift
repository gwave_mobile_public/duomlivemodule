//
//  LiveStickerItemCell.swift
//  DuomLiveModule
//
//  Created by kuroky on 2023/2/16.
//

import Foundation
import DuomBase

class LiveStickerItemCell: UICollectionViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(LiveStickerItemCell.self)
    
    private let imageView: UIImageView = UIImageView().then {
        $0.image = UIImage(named: "sticker_test")
        $0.contentMode = .scaleAspectFit
    }
    
//    let textLabel = UILabel().then {
//        $0.textColor = .grayColor
//        $0.font = .appFont(ofSize: 16)
//    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUI()
    }
    
    private func initUI()  {
        contentView.addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.edges.equalTo(UIEdgeInsets.init(top: 20, left: 20, bottom: 20, right: 20))
        }
        
//        contentView.addSubview(textLabel)
//        textLabel.snp.makeConstraints { make in
//            make.center.equalToSuperview()
//        }
    }
    
    func updateCell(_ img: String)  {
        if let imgUrl = URL(string: img) {
            imageView.kf.setImage(with: imgUrl)
        }
    }
}
