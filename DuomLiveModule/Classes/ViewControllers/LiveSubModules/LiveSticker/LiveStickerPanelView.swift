//
//  LiveStickerPanelView.swift
//  DuomLiveModule
//
//  Created by kuroky on 2023/2/16.
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa

public protocol LiveImageStickerContainerDelegate {
    var selectImageBlock: ((LiveChatStickerModel) -> Void)? { get set }
    var hideBlock: (() -> Void)? { get set }
    func show(in view: UIView)
}

class LiveStickerSegmentButton: UIButton {
    private let imgView = UIImageView().then {
        $0.contentMode = .scaleAspectFill
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setup()  {
        addSubview(imgView)
        imgView.snp.makeConstraints { make in
            make.edges.equalTo(UIEdgeInsets(hAxis: 8, vAxis: 48))
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = isSelected ? UIColor(43, 43, 43) : .clear
        roundCorners(radius: 5)
    }
}

class LiveStickerPanView: UIView, LiveImageStickerContainerDelegate {
    private(set) var containerViewH: CGFloat = 293.DScale
    
    private var containerView: UIView!
    
    private let bag = DisposeBag()
    
    private let collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: UICollectionViewLayout()).then {
        $0.register(LiveStickerItemCell.self, forCellWithReuseIdentifier: LiveStickerItemCell.identifier)
        $0.backgroundColor = .black
        $0.showsVerticalScrollIndicator = false
        $0.showsHorizontalScrollIndicator = false
        $0.isPagingEnabled = true
        $0.isScrollEnabled = true
        $0.emptyView = EmptyView(.emptyButton(title: "GO TO STICKERS STORE"))
    }
    
    private let segmentContainerView = UIView()
    
    private let segmentScrollView = UIScrollView().then {
        $0.showsVerticalScrollIndicator = false
        $0.showsHorizontalScrollIndicator = false
    }
    
    var dataSource: [StickerInfo] = AccountManager.shared.getStickersInfo()
    
    private var segmentButtons: [LiveStickerSegmentButton] = []
    
    private let selectedSegment = BehaviorRelay(value: -1)
    
    var selectImageBlock: ((LiveChatStickerModel) -> Void)?
    
    var hideBlock: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUIView()
        setupSegmentButtons()
        
        selectedSegment.subscribe(onNext: { [unowned self] section in
            if section >= 0, section < self.segmentButtons.count {
                self.segmentButtons.forEach { $0.isSelected = false }
                self.segmentButtons[section].isSelected = true
            }
        })
        .disposed(by: bag)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUIView()
    }
    
    private func setupUIView()  {
        containerView = UIView()
        addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(self.containerViewH)
        }
        
        collectionView.collectionViewLayout = LiveStickerLayout()
        collectionView.delegate = self
        collectionView.dataSource = self
        containerView.addSubview(collectionView)
        containerView.addSubview(segmentContainerView)
        segmentContainerView.addSubview(segmentScrollView)
        
        segmentContainerView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(66)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(segmentContainerView.snp.bottom)
        }
        
        segmentScrollView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(12)
            make.trailing.equalTo(-12)
            make.height.equalTo(36.DScale)
        }
    }
    
    @objc private func tapAction()  {
        hide()
    }
    
    func hide()  {
        self.hideBlock?()
        self.isHidden = true
    }
    
    func show(in view: UIView) {
        if superview !== view {
            removeFromSuperview()
            view.addSubview(self)
            containerViewH = view.bounds.height
            self.snp.makeConstraints { make in
                make.leading.trailing.top.equalToSuperview()
                make.height.equalTo(containerViewH)
            }
        }
        
        self.isHidden = false
        self.containerView.snp.updateConstraints { make in
            make.height.equalTo(containerViewH)
        }
        view.layoutIfNeeded()
        
    }
    
    private func setupSegmentButtons()  {
        segmentScrollView.subviews.forEach { $0.removeFromSuperview() }
        segmentButtons.forEach { $0.removeFromSuperview() }
        segmentButtons = []
        
        let w = 36.DScale as CGFloat
        let h = 36.DScale as CGFloat
        
        segmentScrollView.contentSize = CGSize(width: w * CGFloat(dataSource.count), height: h)
        var tempBtn: LiveStickerSegmentButton!
        for (idx, item) in dataSource.enumerated() {
            tempBtn = LiveStickerSegmentButton(frame: CGRect(x: w * CGFloat(idx) + CGFloat(16 * idx), y: 0, width: w, height: h))
            if let imgStr = self.dataSource[idx].icon, let imgURL = URL(string: imgStr) {
                tempBtn.kf.setImage(with: imgURL, for: .normal)
                tempBtn.kf.setImage(with: imgURL, for: .highlighted)
            }
            segmentScrollView.addSubview(tempBtn)
            segmentButtons.append(tempBtn)
            tempBtn.rx
                .tap
                .subscribe(onNext: { [weak self] in
                    guard let self = self else { return }
                    let pageIndex = CGFloat(self.pageIndexOfSection(idx))
                    self.collectionView.setContentOffset(CGPoint(x: pageIndex * self.collectionView.bounds.width, y: 0), animated: true)
                    self.selectedSegment.accept(idx)
                })
                .disposed(by: bag)
        }
        selectedSegment.accept(0)
    }
    
    private func pageIndexOfSection(_ section: Int) -> Int {
        let pageIndex = (0 ..< section).map {
            let count = CGFloat(self.dataSource[$0].image.count) / CGFloat(6)
            let pageCount = Int(ceil(count))
            return max(1, pageCount)
        }.reduce(0, +)
        return Int(pageIndex)
    }
    
    private func sectionForPage(_ page: Int) -> Int? {
        var accumulativePage = 0
        for (i, section) in dataSource.enumerated() {
            let pageCount = Int(ceil(CGFloat(section.image.count) / CGFloat(6)))
            accumulativePage += max(pageCount, 1)
            if page + 1 <= accumulativePage {
                return i
            }
        }
        return nil
    }
}

extension LiveStickerPanView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LiveStickerItemCell.identifier, for: indexPath) as? LiveStickerItemCell else {
            return UICollectionViewCell()
        }
        cell.updateCell(dataSource[indexPath.section].decryptImages[indexPath.row].decryptOriginal ?? "")
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource[section].image.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var model = LiveChatStickerModel()
        let imageString = self.dataSource[indexPath.section].decryptImages[indexPath.row].decryptOriginal
        model.decryptOriginal = imageString
        selectImageBlock?(model)
    }
    
}

extension LiveStickerPanView: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x / scrollView.frame.width)
        if let section = sectionForPage(page) {
            selectedSegment.accept(section)
            
            if let button = segmentButtons.first(where: { $0.isSelected }) {
                let minX = button.frame.minX + 2
                let maxX = button.frame.maxX - 2
                
                if minX < segmentScrollView.contentOffset.x {
                    segmentScrollView.setContentOffset(CGPoint(x: minX, y: 0), animated: true)
                } else if maxX > segmentScrollView.contentOffset.x + segmentScrollView.frame.width {
                    segmentScrollView.setContentOffset(CGPoint(x: maxX - segmentScrollView.frame.width, y: 0), animated: true)
                }
            }
        }
    }
}
