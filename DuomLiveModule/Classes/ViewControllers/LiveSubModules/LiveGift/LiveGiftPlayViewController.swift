//
//  LiveGiftPlayViewController.swift
//  abseil
//
//  Created by kuroky on 2022/11/9.
//

import UIKit
import DuomBase
import RxCocoa
import RxSwift
import Kingfisher
import YYEVA

class LiveGiftPlayViewController: LiveSubModuleViewController {
    typealias TargetViewModel = LiveViewModel
    typealias Event = LiveViewModel.Action
    
    private let player = YYEVAPlayer().then {
        $0.isUserInteractionEnabled = false
    }
    
    private var datasQueue: [LiveGiftItem] = []
    
    private(set) var isPlaying: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        view.addSubview(player)
        player.delegate = self
        player.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    override func bind(viewModel: LiveViewModel) {
        viewModel.state.bigGiftPlayRelay
            .subscribe(onNext: { [weak self] list in
                guard let self = self else { return }
                self.datasQueue.append(contentsOf: list)
                self.showAnimation()
            })
            .disposed(by: disposeBag)
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
    
    func showAnimation()  {
        guard datasQueue.isNotEmpty else {
            isPlaying = false
            return
        }
        
        if isPlaying {
            return
        } else {
            startPlayFile(datasQueue.first!)
        }
    }
    
    func startPlayFile(_ data: LiveGiftItem)  {
        isPlaying = true
        switch data.level {
        case .high:
            player.play(getVideoLottieSource("高级礼物") ?? "")
        default:
            player.play(getVideoLottieSource("中级礼物") ?? "")
        }
        self.datasQueue.removeFirst()
    }

}

extension LiveGiftPlayViewController: IYYEVAPlayerDelegate {
    func evaPlayerDidCompleted(_ player: YYEVAPlayer) {
        Logger.log(message: "play end", level: .info)
        isPlaying = false
        showAnimation()
    }
}
