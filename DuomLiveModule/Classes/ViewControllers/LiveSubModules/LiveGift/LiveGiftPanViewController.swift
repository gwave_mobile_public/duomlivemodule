//
//  LiveGiftPanViewController.swift
//  abseil
//
//  Created by kuroky on 2022/11/9.
//  礼物面板
//

import Foundation
import UIKit
import DuomBase
import RxSwift
import RxDataSources

class LiveGiftPanViewController: PresentationViewController, ViewType, ResponderChainRouter, LiveRoomSubModule {
    typealias TargetViewModel = LiveViewModel
    typealias Event = LiveViewModel.Action
    
    private let headerView = LiveGiftHeaderView().then {
        $0.backgroundColor = .themeBlackColor
    }
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets.init(top: 13, left: 25, bottom: 0, right: 25)
        layout.minimumLineSpacing = 12
        layout.minimumInteritemSpacing = 30
        layout.itemSize = CGSize(width: (UIScreen.screenWidth - 25 * 2 - 30 * 2) / 3, height: 120.DScale)
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.backgroundColor = .clear
        view.contentInsetAdjustmentBehavior = .never
        view.register(LiveGiftCell.self, forCellWithReuseIdentifier: LiveGiftCell.identifier)
        view.emptyView = EmptyView(.normal(title: "No Data"))
        return view
    }()
    
    private lazy var dataSource = RxCollectionViewSectionedReloadDataSource<LiveGiftSection>.init(configureCell: {
        (datasource, collect, idx, element) -> UICollectionViewCell in
        let cell = collect.dequeueReusableCell(withReuseIdentifier: LiveGiftCell.identifier, for: idx) as! LiveGiftCell
        cell.configGiftModel(element)
        cell.sendBlock = { [weak self] in
            guard let self = self else { return }
//            self.viewModel?.action.onNext(.sendCustomMsg(msg: nil, gift: element, type: .LIVE_GIFT))
            self.viewModel?.action.onNext(.sendGift(gift: element))
            self.dismiss(animated: true)
        }
        return cell
    })
    
    private(set) var selectedIndexpath: IndexPath?
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getLiveCommonViewModel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let selectedIndexpath = selectedIndexpath {
            collectionView.deselectItem(at: selectedIndexpath, animated: true)
        }
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        let containerView = UIView()
        containerView.backgroundColor = .themeBlackColor
        view.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.width.equalTo(UIScreen.screenWidth)
            make.height.equalTo(314.DScale + UIScreen.bottomOffset)
        }
        
        view.addSubview(headerView)
        view.addSubview(collectionView)
        
        headerView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(50)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom)
            make.height.equalTo(314.DScale - 50.DScale + 10)
        }
        
        self.view.layoutIfNeeded()
        self.view.setupCorners(rect: self.view.bounds, corners: [.topLeft, .topRight], radii: .init(width: 15, height: 15))
    }
    
    func bind(viewModel: LiveViewModel) {
        
        collectionView.rx.itemSelected
            .subscribe(onNext: { [weak self] index in
                self?.selectedIndexpath = index
            })
            .disposed(by: disposeBag)
        
        viewModel.state.giftListRelay
            .map { [LiveGiftSection(items: $0)] }
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
    
    
}
