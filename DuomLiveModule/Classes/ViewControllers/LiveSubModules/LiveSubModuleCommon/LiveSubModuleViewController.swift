//
//  LiveSubModuleViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/11/17.
//

import Foundation
import DuomBase

class LiveSubModuleViewController: BaseViewController, ViewType, ResponderChainRouter, LiveRoomSubModule {
    typealias TargetViewModel = LiveViewModel
    typealias Event = LiveViewModel.Action
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getLiveCommonViewModel()
    }
    
    func bind(viewModel: LiveViewModel) {}
}

