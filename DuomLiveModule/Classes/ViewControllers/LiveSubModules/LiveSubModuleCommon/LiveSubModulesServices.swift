//
//  LiveSubModulesServices.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/11/16.
//

import Foundation
import DuomBase

private var lastLiveSubModuleRec = NSMapTable<NSString, AnyObject>.strongToWeakObjects()
private var lastLiveViewModel: LiveViewModel?

private extension ViewControllerSubModule {
    func findRepository<T>(_ type: T.Type) -> T? {
        let recKey = String(describing: type) as NSString
        if let repository = subModuleService.repository as? T {
            lastLiveSubModuleRec.setObject(repository as AnyObject, forKey: recKey)
            return repository
        }
        return lastLiveSubModuleRec.object(forKey: recKey) as? T
    }
}

public protocol LiveRoomSubModule: ViewControllerSubModule, ViewType {
    func getLiveCommonViewModel()
}

extension LiveRoomSubModule where Self: UIViewController {
    var mediaKit: LiveAgoraMedia? {
        return (viewModel as? LiveViewModel)?.mediaKit
    }
    
    var session: LiveSessison? {
        return (viewModel as? LiveViewModel)?.session
    }
    
    // 转成具体的业务vm
    public func getLiveCommonViewModel()  {
        if let repository = findRepository(LiveViewModel.self) {
            lastLiveViewModel = repository
            if let targetViewModel = repository as? TargetViewModel {
                self.viewModel = targetViewModel
            }
            return
        }
        
        if let lastVM = lastLiveViewModel {
            if let targetViewModel = lastVM as? TargetViewModel {
                self.viewModel = targetViewModel
            }
            return
        }
       
        fatalError("not find TargetViewModel in ViewController Chain! \(self)")
    }
}
