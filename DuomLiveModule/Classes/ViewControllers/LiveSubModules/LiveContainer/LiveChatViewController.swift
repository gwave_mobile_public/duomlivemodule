//
//  LiveChatViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/23.
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa
import RxDataSources

class LiveChatViewController: LiveSubModuleViewController {
    private let tableView = UITableView().then {
        $0.backgroundColor = .clear
        $0.showsVerticalScrollIndicator = false
        $0.showsHorizontalScrollIndicator = false
        $0.separatorStyle = .none
        $0.separatorColor = .clear
        $0.rowHeight = UITableView.automaticDimension
        $0.register(LiveChatCell.self, forCellReuseIdentifier: LiveChatCell.identifier)
        $0.register(LiveChatImageCell.self, forCellReuseIdentifier: LiveChatImageCell.identifier)
        $0.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.screenWidth, height: 5))
        $0.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.screenWidth, height: 5))
    }
    
    private let backImgView: UIImageView = UIImageView().then {
        $0.image = UIImage(named: "live_chat_back")
    }
    
    private let dataSource = RxTableViewSectionedReloadDataSource<LiveChatSection> (configureCell: {
        (datasource, tableview, indexPath, element) in
        if element.sticker != nil {
            let cell = tableview.dequeueReusableCell(withIdentifier: LiveChatImageCell.identifier, for: indexPath) as! LiveChatImageCell
            cell.configCellWith(element)
            return cell
        } else {
            let cell = tableview.dequeueReusableCell(withIdentifier: LiveChatCell.identifier, for: indexPath) as! LiveChatCell
            cell.configCellWith(element)
            return cell
        }
    })
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = .clear
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        view.addSubview(backImgView)
        backImgView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(50)
        }
    }
    
    override func bind(viewModel: LiveViewModel) {
        viewModel.state.msgListData
            .map { [LiveChatSection(items: $0)] }
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        viewModel.state.msgListData
            .subscribe(onNext: { [weak self] datas in
                if datas.isNotEmpty {
                    self?.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .bottom, animated: true)
                }
            })
            .disposed(by: disposeBag)
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
}

//extension LiveChatViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//    }
//}

