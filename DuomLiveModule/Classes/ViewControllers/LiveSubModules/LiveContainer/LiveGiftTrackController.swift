//
//  LiveTrackController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2023/2/20.
//

import Foundation

class LiveGiftTrackController: LiveSubModuleViewController {
    private let trackView = LiveGiftTrackContainerView().then {
        $0.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        trackView.isHidden = true
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        view.addSubview(trackView)
        trackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    override func bind(viewModel: LiveViewModel) {
        viewModel.state.giftPlayRelay
            .subscribe(onNext: { [weak self] list in
                guard let self = self else { return }
                self.trackView.applyDatas(list)
                self.trackView.isHidden = false
                self.trackView.showGift()
                self.viewModel?.action.onNext(.clearGiftHistoryMsg)
            })
            .disposed(by: disposeBag)
    }
}
