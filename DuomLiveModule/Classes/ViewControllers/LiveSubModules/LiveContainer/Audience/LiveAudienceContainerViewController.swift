//
//  LiveAudienceContainerViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/24.
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa
import UIKit

class LiveAudienceContainerViewController: LiveSubModuleViewController {
    private let broadcasterButton = DuomButton().then {
        $0.duomAlignment = .default
        $0.padding = 10
    }
    
    private lazy var topRightView = LiveTopRightViewController(role: .Audience)
    
    private lazy var bottomToolsVC = LiveBottomToolsViewController(role: .Audience)
    
    private lazy var chatVC = LiveChatViewController()
    
    private lazy var brocaster = LiveBrocatserView()
    
    private lazy var giftCoverVC = LiveGiftPlayViewController().then {
        $0.view.isUserInteractionEnabled = false
    }
    
    private lazy var joinedVC = LiveBottomScrollController()
    
    private lazy var giftTrackVC = LiveGiftTrackController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = .clear
        view.addSubview(broadcasterButton)
        
        addChild(topRightView)
        view.addSubview(topRightView.view)
        view.bringSubviewToFront(topRightView.view)
        topRightView.didMove(toParent: self)
        
        addChild(bottomToolsVC)
        view.addSubview(bottomToolsVC.view)
        bottomToolsVC.didMove(toParent: self)
        
        addChild(chatVC)
        view.addSubview(chatVC.view)
        chatVC.didMove(toParent: self)
        
        view.addSubview(brocaster)
        
        addChild(giftCoverVC)
        view.addSubview(giftCoverVC.view)
        giftCoverVC.didMove(toParent: self)
        
        addChild(joinedVC)
        view.addSubview(joinedVC.view)
        joinedVC.didMove(toParent: self)
        
        addChild(giftTrackVC)
        view.addSubview(giftTrackVC.view)
        giftTrackVC.didMove(toParent: self)
        
        topRightView.view.snp.makeConstraints { make in
            make.top.equalTo(57.DScale)
            make.trailing.equalTo(-16)
            make.height.equalTo(20.DScale)
            make.width.equalTo(60.DScale)
        }
        
        bottomToolsVC.view.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.height.equalTo(50)
        }
        bottomToolsVC.view.layoutIfNeeded()
        
        brocaster.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.height.equalTo(40)
            make.top.equalTo(55)
        }
       
        giftCoverVC.view.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: UIScreen.screenWidth - 50, height: UIScreen.screenHeight - 50))
        }
        
        joinedVC.view.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.bottom.equalTo(bottomToolsVC.view.snp.top).offset(-12)
            make.height.equalTo(22.DScale)
            make.width.equalTo(250.DScale)
        }
        
        chatVC.view.snp.makeConstraints { make in
            make.bottom.equalTo(joinedVC.view.snp.top).offset(-12)
            make.height.equalTo(250)
            make.leading.equalTo(16)
            make.trailing.equalTo(-66)
        }
        
        giftTrackVC.view.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.height.equalTo(56)
            make.bottom.equalTo(chatVC.view.snp.top)
            make.width.equalTo(235.DScale)
        }
    }
    
    override func bind(viewModel: LiveViewModel) {
        rx.viewWillAppear
            .map { _ in .getLiveInfo}
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
        
        viewModel.state.liveInfoPublish
            .subscribe(onNext: { [weak self] info in
                self?.brocaster.updateLiveInfo(name: info.0, avatar: info.1)
            })
            .disposed(by: disposeBag)
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
    
    
}

extension LiveAudienceContainerViewController {
    func leaveLive() {
        self.viewModel?.action.onNext(.quitLive)
    }
}
