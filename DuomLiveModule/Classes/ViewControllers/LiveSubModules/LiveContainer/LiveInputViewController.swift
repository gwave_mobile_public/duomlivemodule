//
//  LiveInputViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/26.
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa
import RxKeyboard

class LiveInputViewController: BaseViewController {
    typealias InputComletion = ((String) -> Void)
    typealias StickerCompletion = ((LiveChatStickerModel) -> Void)
    
    private let textFiledContainerView: UIView = UIView().then {
        $0.roundCorners(radius: 25)
        $0.backgroundColor = UIColor(44, 44, 44)
    }
    
    private lazy var textField: UITextField = UITextField().then {
        $0.textAlignment = .left
        $0.borderStyle = .none
        $0.font = .appFont(ofSize: 14, fontType: .Inter_Medium)
        $0.textColor = .textWhiteColor
        $0.attributedPlaceholder = placehloder.attributes {
            $0.textColor(UIColor.textWhiteColor)
                .font(.appFont(ofSize: 14, fontType: .Inter_Medium))
        }
        $0.returnKeyType = .done
    }
    
    private lazy var sendButton = DuomThemeButton.init(style: .highLight, title: buttonTitle)
    
    private lazy var changeButton: UIButton = UIButton().then {
        $0.setImage(UIImage(named: "keyboard_smile"), for: .normal)
        $0.setImage(UIImage(named: "keyboard_grid"), for: .selected)
        $0.setEnlargeEdge(UIEdgeInsets(hAxis: 15, vAxis: 15))
    }
    
    private let containerView = UIView().then {
        $0.backgroundColor = .black
    }
    
    private let backView = UIView().then {
        $0.backgroundColor = .black
    }
    
    private lazy var keyInputView = LiveKeyBoardInputView(frame: CGRect(x: 0, y: 0, width: UIScreen.screenWidth, height: keyBoardHeight))
    
    var placehloder: String = "Your Comment..."
    
    var buttonTitle: String = "Send"
    
    var completion: InputComletion?
    
    var stickerCompletion: StickerCompletion?
    
    var keyBoardHeight: CGFloat = 0.0
    
    var isShowingSticker: Bool = false
    
    convenience init(_ buttonTitle: String, placehloder: String, inputCompletion: @escaping InputComletion, stickerCompletion: @escaping StickerCompletion) {
        self.init(nibName: nil, bundle: nil)
        self.placehloder = placehloder
        self.buttonTitle = buttonTitle
        self.completion = inputCompletion
        self.stickerCompletion = stickerCompletion
        setSubViewsConstraints()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textField.becomeFirstResponder()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        textField.resignFirstResponder()
    }
    
    // 不在setupConstraints写 为了解决键盘问题
    private func setSubViewsConstraints()  {
        view.backgroundColor = .clear
        self.view.addSubview(containerView)
        self.view.addSubview(backView)
        containerView.addSubview(textFiledContainerView)
        textFiledContainerView.addSubview(textField)
        textFiledContainerView.addSubview(sendButton)
        textFiledContainerView.addSubview(changeButton)
        
        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(self.view.snp.bottom)
            make.width.equalTo(UIScreen.screenWidth)
            make.height.equalTo(74)
        }
        
        backView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(self.containerView.snp.bottom)
            make.width.equalTo(UIScreen.screenWidth)
            make.height.equalTo(0)
        }
        
        textFiledContainerView.snp.makeConstraints { make in
            make.edges.equalTo(UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12))
        }
        
        sendButton.snp.makeConstraints { make in
            make.trailing.equalTo(-5)
            make.centerY.equalToSuperview()
            make.size.equalTo(CGSize(width: 72, height: 38))
        }
        
        changeButton.snp.makeConstraints { make in
            make.trailing.equalTo(sendButton.snp.leading).offset(-15)
            make.size.equalTo(CGSize(width: 20, height: 20))
            make.centerY.equalToSuperview()
        }

        textField.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalTo(12)
            make.trailing.equalTo(changeButton.snp.leading).offset(-10)
        }
        textField.delegate = self

        RxKeyboard.instance.visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                guard !self.isShowingSticker else { return }
                if keyboardVisibleHeight > 0 {
                    self.containerView.snp.remakeConstraints { make in
                        make.leading.trailing.equalToSuperview()
                        make.bottom.equalTo(-keyboardVisibleHeight)
                        make.width.equalTo(UIScreen.screenWidth)
                        make.height.equalTo(74)
                    }
                    
                    self.backView.snp.remakeConstraints { make in
                        make.leading.trailing.equalToSuperview()
                        make.top.equalTo(self.containerView.snp.bottom)
                        make.width.equalTo(UIScreen.screenWidth)
                        make.height.equalTo(keyboardVisibleHeight)
                    }

                    self.view.setNeedsLayout()
                    self.keyBoardHeight = keyboardVisibleHeight
                    
                    UIView.animate(withDuration: 0.25) {
                        self.view.layoutIfNeeded()
                    }
                } else {
                    self.containerView.snp.remakeConstraints { make in
                        make.leading.trailing.equalToSuperview()
                        make.top.equalTo(self.view.snp.bottom)
                        make.width.equalTo(UIScreen.screenWidth)
                        make.height.equalTo(74)
                    }
                    
                    self.backView.snp.remakeConstraints { make in
                        make.leading.trailing.equalToSuperview()
                        make.top.equalTo(self.containerView.snp.bottom)
                        make.width.equalTo(UIScreen.screenWidth)
                        make.height.equalTo(0)
                    }

                    self.view.setNeedsLayout()

                    UIView.animate(withDuration: 0.25) {
                        self.view.layoutIfNeeded()
                    }
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        sendButton.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.complectionCallback()
            })
            .disposed(by: disposeBag)
        
        changeButton.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.keyBoardChange(self.changeButton)
            })
            .disposed(by: disposeBag)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        dismiss()
    }
    
    private func dismiss()  {
        textField.resignFirstResponder()
        self.dismiss(animated: true)
    }
    
    @objc private func keyBoardChange(_ sender: UIButton)  {
        sender.isSelected.toggle()
        if sender.isSelected {
//            delay(0.3) { [weak self]  in
//                guard let self = self else { return }
//                self.textField.inputView = self.keyInputView
//                self.textField.inputAccessoryView = nil
//                self.textField.reloadInputViews()
//            }
            showStickAnimation()
        } else {
//            delay(0.3) { [weak self] in
//                self?.textField.inputView = nil
//                self?.textField.reloadInputViews()
//                self?.textField.becomeFirstResponder()
//            }
            
            hiddenStickAnimation()
        }
    }
    
    private func showStickAnimation()  {
        isShowingSticker = true
        UIView.animate(withDuration: 0.1) {
            self.textField.resignFirstResponder()
        } completion: { _ in
            self.textField.inputView = self.keyInputView
            self.textField.becomeFirstResponder()
        }
        
        keyInputView.sendStickerBlock = { [weak self] img in
            guard let self = self else { return }
            self.stickerCompletion?(img)
            self.dismiss()
        }
    }
    
    private func hiddenStickAnimation()  {
        UIView.animate(withDuration: 0.1) {
            self.textField.resignFirstResponder()
        } completion: { _ in
            self.textField.inputView = nil
            self.textField.becomeFirstResponder()
            // 延迟赋值
            self.isShowingSticker = false
        }
    }
    
}


extension LiveInputViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        complectionCallback()
        return true
    }
    
    func complectionCallback()  {
        if let content = textField.text, content.isNotEmpty, let complection = self.completion {
            complection(content)
            self.textField.text = nil
        }
        dismiss()
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if let text = textField.text as? NSString {
//            let newString = text.replacingCharacters(in: range, with: string)
//            if newString.count > commentMaxCount {
//                return false
//            }
//        }
//        return true
//    }
    
}

extension LiveInputViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isKind(of: UITapGestureRecognizer.self) {
            return true
        }
        return false
    }
}



class LiveKeyBoardInputView: BaseView {
    private let stickerPanelView = LiveStickerPanView()
    
    var sendStickerBlock: ((LiveChatStickerModel) -> Void)?
    
    override func setupUI() {
        super.setupUI()
        backgroundColor = .black
        
        stickerPanelView.show(in: self)
        
        stickerPanelView.hideBlock = { [weak self] in
            print("hide sticker panel")
        }
        
        stickerPanelView.selectImageBlock = { [weak self] img in
            guard let self = self else { return }
            self.sendStickerBlock?(img)
        }
        
    }
}
