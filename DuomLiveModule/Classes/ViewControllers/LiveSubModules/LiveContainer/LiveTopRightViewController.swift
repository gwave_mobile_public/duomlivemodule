//
//  LiveTopRightViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/22.
//

import Foundation
import DuomBase
import RxCocoa
import RxSwift

class LiveTopRightViewController: BaseViewController {
    private let closeButton = UIButton().then {
        $0.setImage(UIImage(named: "live_close"), for: .normal)
        $0.setImage(UIImage(named: "live_close"), for: .selected)
        $0.setEnlargeEdge(30)
    }
    
    private let liveButton = UIButton().then {
        $0.setImage(UIImage(named: "live_living_icon"), for: .normal)
        $0.setImage(UIImage(named: "live_living_icon"), for: .selected)
    }
    
    var role: LiveRole = .Audience
    
    convenience init(role: LiveRole) {
        self.init()
        self.role = role
    }
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addViewsAction()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = .clear
        view.addSubview(closeButton)
        view.addSubview(liveButton)
        
        closeButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.size.equalTo(CGSize(width: 30, height: 30).DScale)
            make.centerY.equalToSuperview()
        }
        
        liveButton.snp.makeConstraints { make in
            make.trailing.equalTo(closeButton.snp.leading)
            make.size.equalTo(CGSize(width: 32, height: 16).DScale)
            make.centerY.equalToSuperview()
        }
    }
    
    private func addViewsAction()  {
        closeButton.addTarget(self, action: #selector(clickCloseAction(_:)), for: .touchUpInside)
        liveButton.addTarget(self, action: #selector(clickCloseAction(_:)), for: .touchUpInside)
    }
    
    @objc func clickCloseAction(_ sender: UIButton) {
        if self.role == .Audience {
            self.showLeaveAler()
        } else {
            if let parent = self.parent as? LiveContainerViewController {
                parent.snapShotRenderView()
            }
        }
    }
}

extension LiveTopRightViewController {
    private func showLeaveAler()  {
        let alert = LiveRoomAlert.init(title: "Leave LiveStream", message: "Do you want to leave this livestream", cancelTitle: "CANCEL", doneTitle: "LEAVE")
        alert.doneAction = { [weak self] _ in
            if let parent = self?.parent as? LiveAudienceContainerViewController {
                parent.leaveLive()
            }
        }
        self.present(alert, animated: true)
    }
}
