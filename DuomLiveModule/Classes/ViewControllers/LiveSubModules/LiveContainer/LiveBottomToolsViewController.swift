//
//  LiveBottomToolsViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/22.
//

import Foundation
import DuomBase
import UIKit
import Lottie
import RxSwift
import RxCocoa
import SwiftUI

class LiveBottomToolsViewController: LiveSubModuleViewController {
    private let messageView = UIView().then {
        $0.backgroundColor = UIColor(0, 0, 0, 0.5)
        $0.roundCorners(radius: 25)
    }
    
    private let messageButton = DuomButton().then {
        $0.isUserInteractionEnabled = false
        $0.duomAlignment = .default
        $0.padding = 18
        $0.setImage(UIImage(named: "keyboard_smile"), for: .normal)
        $0.setTitleColor(.textWhiteColor, for: .normal)
        $0.setTitle("Your Comment...", for: .normal)
        $0.titleLabel?.font = .appFont(ofSize: 14, fontType: .Inter_Medium)
    }
    
    
    private let zanButton = UIButton().then {
        $0.setImage(UIImage(named: "live_zan"), for: .normal)
        $0.setImage(UIImage(named: "live_zan"), for: .selected)
    }
    
    private let giftButton = UIButton().then {
        $0.setImage(UIImage(named: "live_sendGift"), for: .normal)
        $0.setImage(UIImage(named: "live_sendGift"), for: .selected)
    }
    
    
    private let animationView = AnimationView(name: "zan").then {
        $0.contentMode = .scaleToFill
    }
    
    private lazy var inputVC = LiveInputViewController.init("Send", placehloder: "Your Comment...") { [weak self] text in
        self?.viewModel?.action.onNext(.sendTextMsg(msg: text))
    } stickerCompletion: { [weak self] model in
        self?.viewModel?.action.onNext(.sendCustomMsg(msg: nil, sticker: model, gift: nil, type: .LIVE_STICKER))
    }

    
    private lazy var giftPanVC = LiveGiftPanViewController()
    
    private var isPlaying: Bool = false
    
    var role: LiveRole = .Audience
    
    convenience init(role: LiveRole) {
        self.init()
        self.role = role
    }
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = .clear
        
        if role == .Audience {
            self.view.hstack(messageView, giftButton, zanButton, spacing: 5, alignment: .fill, distribution: .fill)
                .snp.makeConstraints { make in
                    make.bottom.equalToSuperview()
                    make.height.equalTo(50)
                    make.leading.trailing.equalToSuperview()
                }
            
            messageView.snp.makeConstraints { make in
                make.height.equalTo(50).priority(.high)
            }
            
            giftButton.snp.makeConstraints { make in
                make.size.equalTo(CGSize(width: 49, height: 49))
            }
            
            zanButton.snp.makeConstraints { make in
                make.size.equalTo(CGSize(width: 49, height: 49))
            }
            
            messageView.addSubview(messageButton)
            messageButton.snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
                make.leading.equalTo(16)
                make.width.equalTo(140)
            }
            
            view.addSubview(animationView)
            animationView.snp.makeConstraints { make in
                make.bottom.equalTo(self.view.snp.top)
                make.centerX.equalTo(zanButton.snp.centerX)
                make.height.equalTo(150)
                make.width.equalTo(80)
            }
            
            zanButton.rx.tap
                .do(onNext: { [weak self] in
                    self?.showAnimation()
                })
                .throttle(.seconds(5), latest: false, scheduler: MainScheduler.instance)
                .subscribe(onNext: { [weak self] in
                    self?.viewModel?.action.onNext(.sendCustomMsg(msg: zanMessage, type: .ZAN_LIVE))
//                    self?.showAnimation()
                })
                .disposed(by: disposeBag)
            
            giftButton.rx.tap
                .throttle(.seconds(1), scheduler: MainScheduler.instance)
                .subscribe(onNext: { [weak self] in
                    self?.showGiftPan()
                })
                .disposed(by: disposeBag)
            
        } else {
            view.addSubview(messageView)
            messageView.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
            
            messageView.addSubview(messageButton)
            messageButton.snp.makeConstraints { make in
                make.top.bottom.equalToSuperview()
                make.leading.equalTo(16)
                make.width.equalTo(140)
            }
        }
        
        messageView.rx.tapGesture()
            .when(.recognized)
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.inputVC.modalPresentationStyle = .custom
                self.inputVC.view.backgroundColor = .clear
                self.present(self.inputVC, animated: true)
            })
            .disposed(by: disposeBag)
    }
    
    private func showAnimation()  {
        if isPlaying {
            return
        }
        isPlaying = true
        animationView.play { [weak self] finish in
            self?.isPlaying = false
        }
    }
    
    override func bind(viewModel: LiveViewModel) {
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
}

extension LiveBottomToolsViewController {
    private func showGiftPan()  {
        giftPanVC.dimmingColor = .clear
        self.navigationController?.present(giftPanVC, animated: true)
    }
}
