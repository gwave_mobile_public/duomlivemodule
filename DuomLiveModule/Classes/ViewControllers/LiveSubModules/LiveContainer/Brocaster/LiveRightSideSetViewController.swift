//
//  LiveRightSideSetViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/22.
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa

class LiveRightSideSetViewController: LiveSubModuleViewController {
    private let backImageView = UIImageView().then {
        $0.image = UIImage(named: "live_right_backImg")
        $0.contentMode = .scaleToFill
    }
    
    private let cotainerView = UIView()
    
    private let micButton = UIButton().then {
        $0.setImage(UIImage(named: "live_mic_on"), for: .normal)
        $0.setImage(UIImage(named: "live_mic_off"), for: .selected)
        $0.setEnlargeEdge(15)
    }
    
    private let videoButton = UIButton().then {
        $0.setImage(UIImage(named: "live_video_on"), for: .normal)
        $0.setImage(UIImage(named: "live_video_off"), for: .selected)
        $0.setEnlargeEdge(15)
    }
    
    private let reverseButton = UIButton().then {
        $0.setImage(UIImage(named: "live_reverse_icon"), for: .normal)
        $0.setImage(UIImage(named: "live_reverse_icon"), for: .selected)
        $0.setEnlargeEdge(15)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = .clear
        view.addSubview(backImageView)
        view.addSubview(cotainerView)
        
        backImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        micButton.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 32, height: 32))
        }
        
        videoButton.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 32, height: 32))
        }
        
        reverseButton.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 32, height: 32))
        }
        
        cotainerView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(-17)
            make.width.equalTo(35)
            make.height.equalTo(32 * 3 + 30 * 2)
        }
        
        cotainerView.vstack(micButton, videoButton, reverseButton, spacing: 30, alignment: .leading, distribution: .fill)
    }
    
    override func bind(viewModel: LiveViewModel) {
        micButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.micButton.isSelected.toggle()
                viewModel.action.onNext(.enableMic(enable: !self.micButton.isSelected))
            })
            .disposed(by: disposeBag)
        
        videoButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.videoButton.isSelected.toggle()
                viewModel.action.onNext(.enableVideo(enable: !self.videoButton.isSelected))
            })
            .disposed(by: disposeBag)
        
        reverseButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.reverseButton.isSelected.toggle()
                viewModel.action.onNext(.switchFrontCamer)
            })
            .disposed(by: disposeBag)
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
    
    
}
