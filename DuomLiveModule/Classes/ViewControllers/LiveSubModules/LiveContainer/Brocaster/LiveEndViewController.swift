//
//  LiveEndViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/23.
//

import Foundation
import DuomBase
import UIKit
import RxSwift
import RxCocoa

class LiveEndViewController: LiveSubModuleViewController {
    private lazy var snapImageView = UIImageView().then {
        $0.isHidden = true
        $0.contentMode = .scaleToFill
    }
    
    private let placeHolderView = LiveConnectingView()
    
    var image: UIImage = UIImage() {
        didSet {
            snapImageView.isHidden = false
            placeHolderView.isHidden = true
            let context = CIContext(options: nil)
            let inputImage = CIImage.init(image: image)
            let filter = CIFilter(name: "CIGaussianBlur")
            filter?.setValue(inputImage, forKey: kCIInputImageKey)
            filter?.setValue(30, forKey: kCIInputRadiusKey)
            let outputCIImage = filter!.outputImage!
            let rect = CGRect(origin: .zero, size: image.size)
            let cgImage = context.createCGImage(outputCIImage, from: rect)
            let newImage = UIImage(cgImage: cgImage!)
            snapImageView.image = newImage
        }
    }
    
    private let titleLabel = UILabel().then {
        $0.textColor = .textWhiteColor
        $0.font = .appFont(ofSize: 20, fontType: .Inter_Blod)
        $0.numberOfLines = 0
        $0.textAlignment = .center
        $0.text = "Are you sure you want to end your livestream ?"
    }
    
    private let endButton = DuomThemeButton(style: .bothYellow, title: "END NOW")
    
    private let cancelButton = DuomThemeButton(style: .highLight, title: "CANCEL")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addViewAction()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        view.addSubview(placeHolderView)
        view.addSubview(snapImageView)
        view.addSubview(titleLabel)
        
        snapImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        placeHolderView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
        }
        
        view.hstack(endButton, cancelButton, spacing: 15, alignment: .fill, distribution: .fillEqually)
            .snp.makeConstraints { make in
                make.leading.equalTo(16)
                make.trailing.equalTo(-16)
                make.bottom.equalTo(-30)
                make.height.equalTo(36)
            }
        
    }
    
    func addViewAction()  {
        cancelButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.removeFromMainView()
            })
            .disposed(by: disposeBag)
    }
    
    private func removeFromMainView() {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    override func bind(viewModel: LiveViewModel) {
        endButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .map {_ in .endLive }
            .bind(to: viewModel.action)
        
        viewModel.state.endLivePublish
            .subscribe(onNext: { [weak self]  in
                self?.navigationController?.popToRootViewController(animated: true)
            })
            .disposed(by: disposeBag)
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
}
