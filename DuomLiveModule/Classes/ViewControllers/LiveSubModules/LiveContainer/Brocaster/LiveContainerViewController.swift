//
//  LiveContainerViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/22.
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa
import UIKit

class LiveContainerViewController: LiveSubModuleViewController {
    private let broadcasterButton = DuomButton().then {
        $0.duomAlignment = .default
        $0.padding = 10
    }
    
    private lazy var topRightView = LiveTopRightViewController(role: .Broadcaster)
    
    private lazy var rightSideView = LiveRightSideSetViewController()
    
    private lazy var bottomToolsVC = LiveBottomToolsViewController(role: .Broadcaster)
    
    private lazy var endVC = LiveEndViewController()
    
    private lazy var chatVC = LiveChatViewController()
    
    private let tipView = LiveBeginTipView()
    
    private lazy var giftCoverVC = LiveGiftPlayViewController().then {
        $0.view.isUserInteractionEnabled = false
    }
    
    private lazy var joinedVC = LiveBottomScrollController()
    
    private lazy var giftTrackVC = LiveGiftTrackController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = .clear
        view.addSubview(broadcasterButton)
        
        addChild(rightSideView)
        view.addSubview(rightSideView.view)
        rightSideView.didMove(toParent: self)
        
        addChild(topRightView)
        view.addSubview(topRightView.view)
        view.bringSubviewToFront(topRightView.view)
        topRightView.didMove(toParent: self)
        
        addChild(bottomToolsVC)
        view.addSubview(bottomToolsVC.view)
        bottomToolsVC.didMove(toParent: self)
        
        addChild(chatVC)
        view.addSubview(chatVC.view)
        chatVC.didMove(toParent: self)
        
        view.addSubview(tipView)
        
        
        addChild(joinedVC)
        view.addSubview(joinedVC.view)
        joinedVC.didMove(toParent: self)
        
        addChild(giftTrackVC)
        view.addSubview(giftTrackVC.view)
        giftTrackVC.didMove(toParent: self)
        
        addChild(giftCoverVC)
        view.addSubview(giftCoverVC.view)
        giftCoverVC.didMove(toParent: self)
        // 放到最后面
        view.sendSubviewToBack(giftCoverVC.view)
        
        
        topRightView.view.snp.makeConstraints { make in
            make.top.equalTo(57.DScale)
            make.trailing.equalTo(-16)
            make.height.equalTo(20.DScale)
            make.width.equalTo(60.DScale)
        }
        
        rightSideView.view.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.top.bottom.equalToSuperview()
            make.width.equalTo(98.DScale)
        }
        
        bottomToolsVC.view.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.height.equalTo(50)
        }
        bottomToolsVC.view.layoutIfNeeded()

        tipView.snp.makeConstraints { make in
            make.bottom.equalTo(bottomToolsVC.view.snp.top).offset(-12)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.height.equalTo(66)
        }
        
        joinedVC.view.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.bottom.equalTo(bottomToolsVC.view.snp.top).offset(-12)
            make.height.equalTo(22.DScale)
            make.width.equalTo(250.DScale)
        }
        
        chatVC.view.snp.makeConstraints { make in
            make.bottom.equalTo(joinedVC.view.snp.top).offset(-12)
            make.height.equalTo(250)
            make.leading.equalTo(16)
            make.trailing.equalTo(-66)
        }
        
        giftTrackVC.view.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.height.equalTo(56)
            make.bottom.equalTo(chatVC.view.snp.top)
            make.width.equalTo(235.DScale)
        }
        
        giftCoverVC.view.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: UIScreen.screenWidth - 50, height: UIScreen.screenHeight - 50))
        }
    }
    
    override func bind(viewModel: LiveViewModel) {
        viewModel.state.snapStatus
            .subscribe(onNext: { [weak self] succ in
                if succ {
                    self?.addEndView()
                }
            })
            .disposed(by: disposeBag)
        
        rx.viewWillAppear
            .map { _ in .getLiveInfo}
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.resignFirstResponder()
    }
}

extension LiveContainerViewController {
    // showtip
    func showTipView()  {
        delay(5.0) { [weak self] in
            self?.tipView.isHidden = true
            self?.tipView.removeFromSuperview()
        }
    }
    
    // snapShot
    public func snapShotRenderView()  {
        self.viewModel?.action.onNext(.snapShot)
    }
    
   
    // end
    public func addEndView()  {
        addChild(endVC)
        view.addSubview(endVC.view)
        endVC.didMove(toParent: self)
        endVC.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        if let image = readImageFromPath() {
            endVC.image = image
        } else {
            
        }
    }
    
    private func readImageFromPath() -> UIImage?  {
        if let filPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first?.appending("/liveSnapShot.jpg") {
            if let imageData = FileManager.default.contents(atPath: filPath) {
                if let image = UIImage.init(data: imageData) {
                    return image
                }
                return nil
            }
            return nil
        }
        return nil
    }
}

