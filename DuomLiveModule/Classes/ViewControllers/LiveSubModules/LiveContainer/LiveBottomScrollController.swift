//
//  LiveBottomScrollController.swift
//  DuomLiveModule
//
//  Created by kuroky on 2023/2/20.
//

import Foundation

class LiveBottomScrollController: LiveSubModuleViewController {
    private let scrollItemView: LiveScrollItemContainerView = LiveScrollItemContainerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        view.addSubview(scrollItemView)
        scrollItemView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        scrollItemView.isHidden = true
    }
    
    override func bind(viewModel: LiveViewModel) {
        viewModel.state.joinMsgData
            .subscribe(onNext: { [weak self] list in
                guard let self = self else { return }
                self.scrollItemView.applyDatas(list)
                self.scrollItemView.isHidden = false
                self.scrollItemView.showItem()
                viewModel.action.onNext(.clearJoinedHistoryMsg)
            })
            .disposed(by: disposeBag)
    }
}
