//
//  LiveMainViewController.swift
//  Pods
//
//  Created by kuroky on 2022/9/20.
//  用户端直播主页面
// 

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxViewController
import AgoraRtcKit
import DuomBase
import RxKeyboard

public class LiveMainViewController: BaseViewController, ViewType, ResponderChainRouter, LiveRoomSubModule {
    public typealias Event = LiveViewModel.Action
    public typealias TargetViewModel = LiveViewModel
    
    public let renderView = UIView(backgroundColor: .clear)
    
    private let placeHolderView = LiveConnectingView().then {
        $0.tip = hostCloseCameraText
    }
    
    private let closeButton = UIButton().then {
        $0.setEnlargeEdge(15)
        $0.setImage(UIImage(named: "live_close"), for: .normal)
    }
    
    private let settingVC = LiveSettingViewController()
    
    private lazy var containerVC = LiveContainerViewController()
    
    var liveMode: LiveMode = .preMode
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        subModuleService.inject(viewModel)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AuthorizationHelper.checkAuthorizationWithAlert(option: [.cameraPermission, .recordPermission], from: self)
            .andThen(Observable.just(true))
            .subscribe(onError: { [weak self] _ in
                self?.navigationController?.popToRootViewController(animated: true)
            })
            .disposed(by: disposeBag)
        
        // 开启防截屏录制开关
        NotificationCenter.default.post(Notification(name: liveRoomStartNotification))
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // 关闭防截屏录制开关
        NotificationCenter.default.post(Notification(name: liveRoomEndNotification))
    }
    
    public override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = UIColor.themeBlackColor
        view.addSubview(renderView)
        
    
        addChild(settingVC)
        view.addSubview(settingVC.view)
        settingVC.didMove(toParent: self)
        
        addChild(containerVC)
        view.addSubview(containerVC.view)
        containerVC.didMove(toParent: self)
        containerVC.view.isHidden = true
        
        view.addSubview(closeButton)
        
        view.sendSubviewToBack(renderView)
        
        renderView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        closeButton.snp.makeConstraints { make in
            make.leading.equalTo(14.DScale)
            make.size.equalTo(CGSize(width: 12, height: 12).DScale)
            make.top.equalTo(57.DScale)
        }
        
        settingVC.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        containerVC.view.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalToSuperview()
        }
        
        closeButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.mediaKit?.leaveChannel()
                self?.navigationController?.popViewController(animated: true)
            })
            .disposed(by: disposeBag)
        
        // 直播模式 UI变动
        if liveMode == .livingMode {
            settingVC.willMove(toParent: nil)
            settingVC.view.removeFromSuperview()
            settingVC.removeFromParent()
            
            self.viewModel?.action.onNext(.publishStream)
        }
        
        LiveHUD.showLiveHud(from: self.renderView)
    }
    
    public func bind(viewModel: LiveViewModel) {
        self.liveMode = viewModel.liveMode
        
        rx.viewWillAppear
            .map { _ in .startLive }
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
        
        viewModel.state.netErrorPublish
            .subscribe(onNext: { [weak self] in
                delay(2.0) {
                    LiveHUD.hiddenLiveHud()
                    self?.navigationController?.popToRootViewController(animated: true)
                }
            })
            .disposed(by: disposeBag)

        
        viewModel.state.joinChannelStatus
            .subscribe(onNext: { [weak self] uid in
                guard let self = self, let mediaKit = self.mediaKit else { return }
                mediaKit.startRenderLocalVideoStream(id: uid, view: self.renderView)
                LiveHUD.hiddenLiveHud()
            })
            .disposed(by: disposeBag)
        
        // 群组创建成功
        viewModel.state.beginPublishStream
            .subscribe(onNext: { [weak self] in
                self?.containerVC.view.isHidden = false
                self?.containerVC.showTipView()
                self?.closeButton.isHidden = true
            })
            .disposed(by: disposeBag)
        
        viewModel.state.hiddeRender
            .subscribe(onNext: { [weak self] hidden in
                self?.setPlaceHolderViewStatus(hidden)
            })
            .disposed(by: disposeBag)
       
        rx.viewWillDisappear
            .map {_ in .deinitAction}
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
}

extension LiveMainViewController {
    // 主播关闭了推流
    private func setPlaceHolderViewStatus(_ show: Bool)  {
        if show {
            renderView.addSubview(placeHolderView)
            placeHolderView.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
        } else {
            placeHolderView.removeFromSuperview()
        }
    }
}

