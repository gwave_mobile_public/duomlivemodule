//
//  LiveChatCell.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/23.
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa

class LiveChatCell: BaseTableViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(LiveChatCell.self)
    
    private let headerView = RoundImageView().then {
        $0.image = UIImage(named: "live_user_placeholder")
        $0.contentMode = .scaleAspectFill
    }
    
    private let nameLabel = UILabel().then {
        $0.textColor = .textWhiteColor
        $0.font = .appFont(ofSize: 13, fontType: .Inter_Medium)
        $0.textAlignment = .left
    }
    
    private let contentLabel = UILabel().then {
        $0.textColor = .textWhiteColor
        $0.font = .appFont(ofSize: 13, fontType: .Inter_Medium)
        $0.textAlignment = .left
        $0.numberOfLines = 3
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        initUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initUI()  {
        self.backgroundColor = .clear
        contentView.backgroundColor = .clear
        contentView.addSubview(headerView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(contentLabel)
        
        headerView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.size.equalTo(CGSize(width: 40, height: 40))
            make.top.equalTo(4)
            make.bottom.equalTo(-4).priority(.low)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.top)
            make.trailing.equalTo(-16)
            make.leading.equalTo(headerView.snp.trailing).offset(9)
            make.height.equalTo(18)
        }
        
        contentLabel.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom)
            make.leading.equalTo(nameLabel.snp.leading)
            make.trailing.equalTo(nameLabel.snp.trailing)
            make.bottom.equalTo(-4).priority(.high)
        }
    }
    
    func configCellWith(_ chat: Chat)  {
        self.nameLabel.text = chat.name
        self.contentLabel.attributedText = chat.content
        if let ava = chat.image {
            headerView.kf.setImage(with: URL(string: ava))
        } else {
            headerView.image = UIImage(named: "live_user_placeholder")
        }
    }
}



class LiveChatImageCell: BaseTableViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(LiveChatImageCell.self)
    
    private let headerView = RoundImageView().then {
        $0.image = UIImage(named: "live_user_placeholder")
        $0.contentMode = .scaleAspectFill
    }
    
    private let nameLabel: UILabel = UILabel().then {
        $0.textColor = .textWhiteColor
        $0.font = .appFont(ofSize: 13, fontType: .Inter_Medium)
        $0.textAlignment = .left
    }
    
    private let contentImage: UIImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFit
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        initUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initUI()  {
        self.backgroundColor = .clear
        contentView.backgroundColor = .clear
        contentView.addSubview(headerView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(contentImage)
        
        headerView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.size.equalTo(CGSize(width: 40, height: 40))
            make.top.equalTo(4)
            make.bottom.equalTo(-4).priority(.low)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.top)
            make.trailing.equalTo(-16)
            make.leading.equalTo(headerView.snp.trailing).offset(9)
            make.height.equalTo(18)
        }
        
        contentImage.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom)
            make.leading.equalTo(nameLabel.snp.leading)
            make.width.equalTo(100.DScale)
            make.height.equalTo(100.DScale)
            make.bottom.equalToSuperview()
        }
    }
    
    func configCellWith(_ chat: Chat)  {
        self.nameLabel.text = chat.name
       
        if let ava = chat.image {
            headerView.kf.setImage(with: URL(string: ava))
        } else {
            headerView.image = UIImage(named: "live_user_placeholder")
        }
        
        if let sticker = chat.sticker, let stickerImg = sticker.decryptOriginal {
            self.contentImage.kf.setImage(with: URL(string: stickerImg))
        }
    }
}
