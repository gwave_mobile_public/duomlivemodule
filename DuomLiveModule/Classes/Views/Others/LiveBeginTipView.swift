//
//  LiveBeginTipView.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/24.
//

import Foundation
import DuomBase

class LiveBeginTipView: BaseView {
    private let iconView = UIImageView().then {
        $0.image = UIImage(named: "live_anchor_tip")
    }
    
    private let tipLabel = UILabel().then {
        $0.textColor = .textWhiteColor
        $0.font = .appFont(ofSize: 14, fontType: .Inter_Medium)
        $0.text = "We'er telling your followers that you’ve started a livestream"
        $0.numberOfLines = 0
        $0.textAlignment = .left
    }
    
    override func setupUI() {
        self.backgroundColor = UIColor(58, 58, 58)
        self.roundCorners(radius: 10)
        
        self.addSubview(iconView)
        self.addSubview(tipLabel)
        
        iconView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(16)
            make.size.equalTo(CGSize(width: 24, height: 24))
        }
        
        tipLabel.snp.makeConstraints { make in
            make.leading.equalTo(iconView.snp.trailing).offset(12)
            make.top.equalTo(13)
            make.bottom.equalTo(-13)
            make.trailing.equalTo(-15)
        }
    }
    
}
