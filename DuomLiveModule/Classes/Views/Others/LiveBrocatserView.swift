//
//  LiveBrocatserView.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/25.
//

import Foundation
import DuomBase
import Kingfisher

class LiveBrocatserView: BaseView {
    private let headerImageView = UIImageView(image: UIImage(named: "live_user_placeholder")).then {
        $0.roundCorners(radius: 20)
        $0.contentMode = .scaleAspectFill
    }
    
    private let nameLabel = UILabel().then {
        $0.textColor = .textWhiteColor
        $0.font = .appFont(ofSize: 14, fontType: .Inter_Blod)
        $0.textAlignment = .left
    }
    
    override func setupUI() {
        self.backgroundColor = .clear
        
        self.addSubview(headerImageView)
        self.addSubview(nameLabel)
        
        headerImageView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.size.equalTo(CGSize(width: 40, height: 40))
            make.leading.equalToSuperview()
        }
        
        nameLabel.snp.makeConstraints { make in
            make.leading.equalTo(headerImageView.snp.trailing).offset(8)
            make.centerY.equalToSuperview()
            make.height.equalTo(20)
        }
    }
    
    func updateLiveInfo(name: String, avatar: String)  {
        self.nameLabel.text = name
        if !avatar.isEmpty {
            self.headerImageView.kf.setImage(with: URL(string: avatar), placeholder: UIImage(named: "live_user_placeholder"))
        }
    }
    
}
