//
//  LiveConnectingView.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/10/20.
//

import Foundation
import DuomBase

class LiveConnectingView: BaseView {
    private let backView = UIImageView(image: UIImage(named: "live_hidden_back"))
    
    private let connectView = UIImageView(image: UIImage(named: "live_connecting"))
    
    private let tipLabel = UILabel().then {
        $0.text = "Connecting..."
        $0.textColor = UIColor.init(217, 217, 217)
        $0.font = .appFont(ofSize: 16, fontType: .Inter_Medium)
        $0.textAlignment = .center
    }
    
    var tip: String? {
        didSet {
            tipLabel.text = tip
        }
    }
    
    override func setupUI() {
        addSubview(backView)
        backView.addSubview(connectView)
        backView.addSubview(tipLabel)
        
        backView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        connectView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: 90, height: 130).DScale)
        }
        
        tipLabel.snp.makeConstraints { make in
            make.top.equalTo(connectView.snp.bottom).offset(15)
            make.centerX.equalToSuperview()
            make.height.equalTo(18.DScale)
        }
    }
}
