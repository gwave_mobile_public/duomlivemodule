//
//  LiveInputView.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/22.
//

import Foundation
import DuomBase

class LiveInputView: BaseView {
    private let emojiImageView = UIImageView().then {
        $0.image = UIImage(named: "keyboard_smile")
    }
    
    private let textField = UITextField().then {
        $0.textAlignment = .left
        $0.borderStyle = .none
        $0.font = .appFont(ofSize: 14, fontType: .Inter_Medium)
        $0.textColor = .textWhiteColor
        $0.attributedPlaceholder = "Your Comment...".attributes {
            $0.textColor(UIColor.textWhiteColor)
                .font(.appFont(ofSize: 14, fontType: .Inter_Medium))
        }
        $0.returnKeyType = .done
    }
    
    private let toolView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.screenWidth, height: 50)).then {
        $0.backgroundColor = .clear
    }
    
    private let sendButton = DuomThemeButton.init(style: .highLight, title: "Send")
    
    var viewModel: LiveViewModel?
    
    override func setupUI() {
        super.setupUI()
        
        self.roundCorners(radius: 30)
        self.backgroundColor = UIColor(0, 0, 0, 0.5)
        addSubview(emojiImageView)
        addSubview(textField)
        textField.delegate = self
        
        emojiImageView.snp.makeConstraints { make in
            make.leading.equalTo(15)
            make.centerY.equalToSuperview()
            make.size.equalTo(CGSize(width: 18, height: 18).DScale)
        }
        
        textField.snp.makeConstraints { make in
            make.leading.equalTo(emojiImageView.snp.trailing).offset(15)
            make.trailing.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
    }
    
    
    func bindViewModel(_ viewModel: LiveViewModel?)  {
        self.viewModel = viewModel
    }
    
}

extension LiveInputView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if let content = textField.text, content.isNotEmpty, let viewModel = self.viewModel {
            viewModel.action.onNext(.sendTextMsg(msg: content))
        }
        self.textField.text = nil
        return true
    }
    
}
