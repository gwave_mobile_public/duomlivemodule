//
//  LiveRoomAlert.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/25.
//

import Foundation
import DuomBase

public class LiveRoomAlert: DuomAlertViewController {
    public var doneAction: ((DuomAlertButtonItem) -> Void)?
    
    public var cancelAction: ((DuomAlertButtonItem) -> Void)?
    
    private let titleLabel = UILabel().then {
        $0.font = UIFont.appFont(ofSize: 16, fontType: .Inter_Blod)
        $0.textColor = UIColor.textWhiteColor
        $0.numberOfLines = 0
        $0.textAlignment = .left
    }
    
    private let messageLabel = UILabel().then {
        $0.font = UIFont.appFont(ofSize: 14, fontType: .Inter_Medium)
        $0.textColor = UIColor.textWhiteColor
        $0.numberOfLines = 0
        $0.textAlignment = .left
    }
    
    private var fixedHeight: CGFloat?
    
    public init(title: String?, message: String?, cancelTitle: String? = nil, doneTitle: String? = nil, fixedHeight: CGFloat? = nil) {
        super.init(transitionStyle: .zoomIn, enableGesture: false)
        
        titleLabel.text = title
        
        messageLabel.text = message
        
        self.fixedHeight = fixedHeight
        
        if let doneTitle = doneTitle {
            buttonItems.append(.default(title: doneTitle))
        }
        
        if let cancelTitle = cancelTitle {
            buttonItems.append(.cancel(title: cancelTitle))
        }
    }
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(titleLabel)
        view.addSubview(messageLabel)
        
        layoutSubviews()
        
        buttonsHorizontalLayout()
    }
    
    
    private func layoutSubviews()  {
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(24.DScale)
            make.leading.equalTo(16.DScale)
            make.trailing.equalTo(-16.DScale)
            make.height.equalTo(19.DScale)
        }
        
        messageLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(16.DScale)
            make.leading.equalTo(16.DScale)
            make.trailing.equalTo(-17.DScale)
            make.bottom.equalTo(buttonContainer.snp.top).offset(-24.DScale)
            if let fixHeight = fixedHeight {
                make.height.equalTo(fixHeight).priority(900)
            }
        }
    }
    
    public override func containerViewDidTap(_ sender: UITapGestureRecognizer) {
        
    }
    
    @objc public override func buttonOnTap(_ sender: UIButton) {
        let item = buttonItems[sender.tag]
        
        if item.dismissOnTap {
            dismiss(animated: true, completion: { [weak self] in
                if item.style == .cancel {
                    self?.cancelAction?(item)
                } else if item.style == .default {
                    self?.doneAction?(item)
                }
            })
        }
    }
    
}

