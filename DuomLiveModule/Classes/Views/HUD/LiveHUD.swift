//
//  LiveHUD.swift
//  Alamofire
//
//  Created by kuroky on 2022/10/19.
//

import Foundation
import DuomBase
import Lottie

class LiveHUD {
    static let liveAnimationView = AnimationView(name: "live_data").then {
        $0.contentMode = .scaleToFill
        $0.loopMode = .loop
    }
    
    static let containerView = UIView().then {
        $0.backgroundColor = .themeBlackColor
    }
    
    static func showLiveHud(from parentView: UIView)  {
        parentView.addSubview(containerView)
        containerView.addSubview(liveAnimationView)
        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        liveAnimationView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview().offset(-15.DScale)
            make.size.equalTo(CGSize(width: 120.DScale, height: 120.DScale))
        }
        
        liveAnimationView.play()
    }
    
    static func hiddenLiveHud()  {
        UIView.animate(withDuration: 0.3) {
            liveAnimationView.stop()
            containerView.removeFromSuperview()
        }
    }
}
