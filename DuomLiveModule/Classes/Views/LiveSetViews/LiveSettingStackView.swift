//
//  LiveSettingStackView.swift
//  Differentiator
//
//  Created by kuroky on 2022/9/21.
//

import Foundation
import DuomBase
import RxCocoa
import RxSwift

class LiveSettingStackView: BaseView {
    var setTitleBlock: (() -> Void)?
    
    var otherSetBlock: (() -> Void)?
    
    private let disposeBag = DisposeBag()
    
    private let titleButton = DuomButton().then {
        $0.duomAlignment = .default
        $0.padding = 8
        $0.setEnlargeEdge(15)
        $0.setImage(UIImage(named: "live_title_icon"), for: .normal)
        $0.setTitle("Title", for: .normal)
        $0.titleLabel?.font = .appFont(ofSize: 12, fontType: .Inter_Medium)
        $0.addTarget(self, action: #selector(clickTitleAction(_:)), for: .touchUpInside)
    }
    
    private let otherButton = DuomButton().then {
        $0.duomAlignment = .default
        $0.padding = 8
        $0.setEnlargeEdge(15)
        $0.setImage(UIImage(named: "live_title_icon"), for: .normal)
        $0.setTitle("Other Settings", for: .normal)
        $0.titleLabel?.font = .appFont(ofSize: 12, fontType: .Inter_Medium)
        $0.addTarget(self, action: #selector(clickSetAction(_:)), for: .touchUpInside)
    }
    
    override func setupUI() {
        super.setupUI()
        self.backgroundColor = .blue
        
        self.vstack(titleButton, otherButton, spacing: 26, alignment: .leading, distribution: .fillEqually)
    }
    
    @objc func clickTitleAction(_ sender: UIButton)  {
        if let titleAction = self.setTitleBlock {
            titleAction()
        }
    }
    
    @objc func clickSetAction(_ sender: UIButton)  {
        if let otherAction = self.otherSetBlock {
            otherAction()
        }
    }
    
}
