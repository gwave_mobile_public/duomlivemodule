//
//  LiveSetCameraSideCell.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/21.
//

import Foundation
import DuomBase
import RxCocoa
import RxSwift

class LiveSetCameraSideCell: BaseTableViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(LiveSetCameraSideCell.self)
    private let leftTitleLabel = UILabel().then {
        $0.textColor = UIColor.textWhiteColor
        $0.textAlignment = .left
        $0.font = .appFont(ofSize: 16, fontType: .Inter_Medium)
        $0.text = "Left Side"
    }
    
    private let rightTitleLabel = UILabel().then {
        $0.textColor = UIColor.textWhiteColor
        $0.textAlignment = .left
        $0.font = .appFont(ofSize: 16, fontType: .Inter_Medium)
        $0.text = "Right Side"
    }
    
    private let leftSideButton = DuomButton().then {
        $0.setImage(UIImage(named: "live_set_side_normal"), for: .normal)
        $0.setImage(UIImage(named: "live_set_side_selected"), for: .selected)
        $0.isUserInteractionEnabled = false
    }
    
    private let rightSideButton = UIButton().then {
        $0.setImage(UIImage(named: "live_set_side_normal"), for: .normal)
        $0.setImage(UIImage(named: "live_set_side_selected"), for: .selected)
        $0.isUserInteractionEnabled = false
    }
    
    private let leftCoverButton = UIButton()
    
    private let rightCoverButton = UIButton()
    
    private let disposeBag = DisposeBag()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func initUI()  {
        contentView.backgroundColor = .themeBlackColor
        
        contentView.addSubview(leftTitleLabel)
        contentView.addSubview(rightTitleLabel)
        contentView.addSubview(leftSideButton)
        contentView.addSubview(rightSideButton)
        contentView.addSubview(leftCoverButton)
        contentView.addSubview(rightCoverButton)
        
        leftTitleLabel.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.top.equalTo(12.DScale)
            make.height.equalTo(19.DScale)
        }
        
        leftSideButton.snp.makeConstraints { make in
            make.centerY.equalTo(leftTitleLabel.snp.centerY)
            make.trailing.equalTo(-16)
            make.size.equalTo(CGSize(width: 24, height: 24).DScale)
        }
        
        rightTitleLabel.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.height.equalTo(19.DScale).priority(.high)
            make.top.equalTo(leftTitleLabel.snp.bottom).offset(24.DScale)
            make.bottom.equalTo(-12.DScale)
        }
        
        rightSideButton.snp.makeConstraints { make in
            make.centerY.equalTo(rightTitleLabel.snp.centerY)
            make.trailing.equalTo(-16)
            make.size.equalTo(CGSize(width: 24, height: 24).DScale)
        }
        
        leftCoverButton.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.top.equalToSuperview()
            make.height.equalTo(44.DScale)
        }
        
        rightCoverButton.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.bottom.equalToSuperview()
            make.height.equalTo(44.DScale)
        }
        
        leftCoverButton.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.leftSideButton.isSelected = true
                self?.rightSideButton.isSelected = false
                self?.emitRouterEvent(LiveSetCameraViewController.Event.switchSide(isLeft: true))
                LiveSettingInstance.shared.enableMirrod = false
            })
            .disposed(by: disposeBag)
        
        rightCoverButton.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.leftSideButton.isSelected = false
                self?.rightSideButton.isSelected = true
                self?.emitRouterEvent(LiveSetCameraViewController.Event.switchSide(isLeft: false))
                LiveSettingInstance.shared.enableMirrod = true
            })
            .disposed(by: disposeBag)
            
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(isLeft: Bool)  {
        self.leftSideButton.isSelected = isLeft
        self.rightSideButton.isSelected = !isLeft
    }
    
}
