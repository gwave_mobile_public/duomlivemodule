//
//  LiveSetCameraSwitchCell.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/21.
//

import Foundation
import DuomBase
import DuomBase
import RxCocoa
import RxSwift

class LiveSetCameraSwitchCell: BaseTableViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(LiveSetCameraSwitchCell.self)
    private let titleLabel = UILabel().then {
        $0.text = "Front Camera"
        $0.textAlignment = .left
        $0.font = .appFont(ofSize: 16, fontType: .Inter_Medium)
        $0.textColor = UIColor.textWhiteColor
    }
    
    private let switchButton = UIButton().then {
        $0.setImage(UIImage(named: "live_set_camera_open"), for: .selected)
        $0.setImage(UIImage(named: "live_set_camera_close"), for: .normal)
        $0.isUserInteractionEnabled = false
    }
    
    private let coverButton = UIButton()
    
    private let disposeBag = DisposeBag()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func initUI()  {
        contentView.backgroundColor = UIColor.themeBlackColor
        contentView.addSubview(switchButton)
        contentView.addSubview(titleLabel)
        contentView.addSubview(coverButton)
        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.centerY.equalToSuperview()
            make.height.equalTo(19.DScale)
        }
        
        switchButton.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(-16)
            make.size.equalTo(CGSize(width: 44, height: 24).DScale)
        }
        
        coverButton.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        coverButton.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
//                self.coverButton.isSelected.toggle()
                self.switchButton.isSelected.toggle()
                print("\(self.switchButton.isSelected)")
                self.emitRouterEvent(LiveSetCameraViewController.Event.switchFrontCamer)
                LiveSettingInstance.shared.cameraIsFront = self.switchButton.isSelected
            })
            .disposed(by: disposeBag)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
//        switchButton.isSelected.toggle()
//        self.emitRouterEvent(LiveSetCameraViewController.Event.switchFrontCamer(isFront: switchButton.isSelected))
    }
    
    func configCell(isSelected: Bool) {
        self.switchButton.isSelected = isSelected
    }
    
}
