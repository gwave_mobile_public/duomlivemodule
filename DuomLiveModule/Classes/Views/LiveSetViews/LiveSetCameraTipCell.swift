//
//  LiveSetCameraTipCell.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/9/21.
//

import Foundation
import DuomBase

class LiveSetCameraTipCell: BaseTableViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(LiveSetCameraTipCell.self)
    private let titleLabel = UILabel().then {
        $0.textColor = UIColor.textWhiteColor
        $0.textAlignment = .left
        $0.font = .appFont(ofSize: 16, fontType: .Inter_Medium)
        $0.text = "Camera Tools"
    }
    
    private let tipLabel = UILabel().then {
        $0.textColor = UIColor(201, 201, 201)
        $0.textAlignment = .left
        $0.numberOfLines = 0
        $0.font = .appFont(ofSize: 12, fontType: .Inter_Medium)
        $0.text = "Choose which side of the screen you want your camera toolbar to be in."
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func initUI()  {
        contentView.backgroundColor = UIColor.themeBlackColor
        contentView.addSubview(titleLabel)
        contentView.addSubview(tipLabel)
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(12.DScale)
            make.leading.equalTo(16)
            make.height.equalTo(19.DScale)
        }
        
        tipLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(6.DScale)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.bottom.equalTo(-12.DScale)
        }
    }
    
}
