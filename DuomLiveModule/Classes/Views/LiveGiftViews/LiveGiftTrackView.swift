//
//  LiveGiftTrackView.swift
//  DuomLiveModule
//
//  Created by kuroky on 2023/2/14.
//

import Foundation
import DuomBase

protocol LiveGiftTrackProtocol {
    var giftUserName: String? { get }
    var gitfUserAvatar: String? { get }
    var giftImageUrl: String? { get }
    var giftName: String? { get }
}

public class LiveGiftTrackView: BaseView {
    private let backImageView: UIImageView = UIImageView().then {
        $0.image = UIImage(named: "gift_trans_red_back")
    }
    
    private let headerImageView: UIImageView = UIImageView().then {
        $0.roundCorners(radius: 20)
    }
    
    private let nameLabel: UILabel = UILabel().then {
        $0.textColor = .white
        $0.font = .appFont(ofSize: 13, fontType: .Inter_Blod)
        $0.textAlignment = .left
    }
    
    private let sendLabel: UILabel = UILabel().then {
        $0.textColor = UIColor(227, 227, 227)
        $0.font = .appFont(ofSize: 12)
        $0.textAlignment = .left
        $0.text = "Send"
    }
    
    private let giftLabel: UILabel = UILabel().then {
        $0.textColor = .themeColor
        $0.font = .appFont(ofSize: 12, fontType: .Inter_Blod)
        $0.textAlignment = .left
    }
    
    private let giftImageView: UIImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
    }
    
    var data: Chat? {
        didSet {
            nameLabel.text = data?.name ?? ""
            headerImageView.kf.setImage(with: URL(string: data?.image ?? ""))
            giftLabel.text = data?.gift?.name ?? ""
            giftImageView.kf.setImage(with: URL(string: data?.gift?.icon ?? ""))
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
//        setupCorners(rect: bounds, corner: CornerConfig.init(topRight: bounds.height / 2, bottomRight: bounds.height / 2))
        
//        gradient(colors: [UIColor(0, 0, 0), UIColor(206, 218, 57)])
    }
    
    public override func setupUI() {
        addSubview(backImageView)
        addSubview(headerImageView)
        addSubview(nameLabel)
        addSubview(sendLabel)
        addSubview(giftLabel)
        addSubview(giftImageView)
        
        backImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        headerImageView.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 40, height: 40))
            make.leading.equalTo(16)
            make.centerY.equalToSuperview()
        }
        
        giftImageView.snp.makeConstraints { make in
            make.bottom.top.equalToSuperview()
            make.trailing.equalTo(-22)
            make.width.equalTo(51.DScale)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(headerImageView.snp.top)
            make.leading.equalTo(headerImageView.snp.trailing).offset(5)
            make.height.equalTo(18)
            make.trailing.equalTo(giftImageView.snp.leading)
        }
        
        sendLabel.snp.makeConstraints { make in
            make.leading.equalTo(nameLabel.snp.leading)
            make.top.equalTo(nameLabel.snp.bottom).offset(2)
            make.height.equalTo(18)
            make.width.lessThanOrEqualTo(30.DScale)
        }
        
        giftLabel.snp.makeConstraints { make in
            make.leading.equalTo(sendLabel.snp.trailing).offset(2)
            make.centerY.equalTo(sendLabel.snp.centerY)
            make.height.equalTo(18)
            make.trailing.equalTo(giftImageView.snp.leading)
        }
        
       
    }
}

class LiveGiftTrackContainerView: BaseView {
    private let itemView = LiveGiftTrackView()
    
    private var datasQueue: [Chat] = []
    
    private var timer: Timer? = nil
    
    private(set) var isPlaying: Bool = false
    
    override func setupUI() {
        super.setupUI()
        
        addSubview(itemView)
        itemView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.trailing.equalTo(self.snp.leading)
            make.width.equalToSuperview()
        }
        
        layoutIfNeeded()
    }
    
    func applyDatas(_ list: [Chat])  {
        self.datasQueue.append(contentsOf: list)
    }
    
    func showGift()  {
        guard datasQueue.isNotEmpty else { return }
        
        if isPlaying {
            return
        } else {
            startAnimation(data: datasQueue.first)
        }
    }
    
    private func startAnimation(data: Chat?)  {
        print("......<<<<<<<<<< \(datasQueue)")
        guard let data = data, let gift = data.gift else {
            isPlaying = false
            return
        }
        
        isPlaying = true
        itemView.data = data
        
        let transX: CGFloat = 235.DScale
        let transY: CGFloat = -56
        
        UIView.animate(withDuration: 0.25) {
            self.itemView.transform = CGAffineTransform(translationX: transX, y: 0)
        }
        
        var duration: Int32 = 2
        if let dura = gift.animationTime, dura > 0 {
            duration = dura
        }
        delay(TimeInterval(duration)) {
            UIView.animate(withDuration: 0.25) {
                self.itemView.transform = self.itemView.transform.translatedBy(x: 0, y: transY)
                self.itemView.alpha = 0
            } completion: { _ in
                self.itemView.alpha = 1
                self.itemView.isHidden = false
                self.itemView.transform = self.itemView.transform.translatedBy(x: -transX, y: -transY)
                self.datasQueue.removeFirst()
                self.startAnimation(data: self.datasQueue.first)
            }
        }
    }
}


