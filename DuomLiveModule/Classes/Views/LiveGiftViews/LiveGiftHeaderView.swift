//
//  LiveGiftHeaderView.swift
//  DuomLiveModule
//
//  Created by kuroky on 2022/11/11.
//

import UIKit
import DuomBase

class LiveGiftHeaderView: BaseView {
    private let giftLabel = UILabel().then {
        $0.textColor = .white
        $0.text = "Send Gift"
        $0.font = .appFont(ofSize: 12, fontType: .Inter_Blod)
        $0.textAlignment = .center
    }
    
//    private let lineView = UIView().then {
//        $0.backgroundColor = .white
//    }
    
    override func setupUI() {
        super.setupUI()
        
        addSubview(giftLabel)
//        addSubview(lineView)
        
        giftLabel.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.height.equalTo(20)
            make.top.equalTo(15)
        }
        
//        lineView.snp.makeConstraints { make in
//            make.centerX.equalTo(giftLabel.snp.centerX)
//            make.width.equalTo(23)
//            make.height.equalTo(3)
//            make.bottom.equalToSuperview()
//            make.top.equalTo(giftLabel.snp.bottom).offset(12)
//        }
    }
    
    func updateContent(_ name: String)  {
        giftLabel.text = "Send Gift TO "
    }

}
