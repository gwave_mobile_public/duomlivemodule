//
//  LiveGiftCell.swift
//  Alamofire
//
//  Created by kuroky on 2022/11/9.
//

import UIKit
import DuomBase

class LiveGiftCell: UICollectionViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(LiveGiftCell.self)
    private let giftImageView: UIImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
    }
    
    private let nameLabel = UILabel().then {
        $0.textColor = .white
        $0.textAlignment = .center
        $0.font = .appFont(ofSize: 13, fontType: .Inter_Blod)
    }
    
    private let priceLabel = UILabel().then {
        $0.textColor = UIColor(119, 119, 117)
        $0.textAlignment = .center
        $0.font = .appFont(ofSize: 10, fontType: .Inter_Blod)
    }
    
    private let gemsIcon: UIImageView = UIImageView().then {
        $0.image = UIImage(named: "gift_gems")
    }
    
    private let sendButton: UIButton = UIButton().then {
        $0.setEnlargeEdge(UIEdgeInsets.init(top: 50, left: 0, bottom: 0, right: 0))
        $0.setTitle("SEND", for: .normal)
        $0.setTitle("SEND", for: .highlighted)
        $0.setTitleColor(.black, for: .normal)
        $0.setTitleColor(.black, for: .highlighted)
        $0.backgroundColor = UIColor(204, 255, 1)
        $0.titleLabel?.font = .appFont(ofSize: 12, fontType: .Inter_Blod)
        $0.isHidden = true
        $0.isUserInteractionEnabled = false
    }
    
    private var stackView: UIStackView = UIStackView()
    
    private lazy var tapGesture: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.sendAction(_:)))
        return tap
    }()
    
    var sendBlock: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override var isSelected: Bool {
        didSet {
            layer.borderColor = isSelected ? UIColor(204, 255, 1).cgColor : UIColor.clear.cgColor
            sendButton.isHidden = !isSelected
            nameLabel.isHidden = isSelected
            if isSelected {
                roundCorners(radius: 10)
                layer.borderWidth = 1
                stackView.snp.remakeConstraints { make in
                    make.centerX.equalToSuperview()
                    make.bottom.equalTo(sendButton.snp.top)
                    make.height.equalTo(15.DScale)
                }
                priceLabel.textColor = .white
                layoutIfNeeded()
                addGestureRecognizer(tapGesture)
            } else {
                roundCorners(radius: 0)
                layer.borderWidth = 0
                stackView.snp.remakeConstraints { make in
                    make.top.equalTo(nameLabel.snp.bottom).offset(2)
                    make.height.equalTo(15.DScale)
                    make.centerX.equalToSuperview()
                }
                priceLabel.textColor = UIColor(119, 119, 117)
                layoutIfNeeded()
                removeGestureRecognizer(tapGesture)
            }
        }
    }
    
    private func initUI()  {
        contentView.addSubview(giftImageView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(sendButton)
        
        giftImageView.snp.makeConstraints { make in
            make.top.equalTo(4)
            make.leading.equalTo(4)
            make.trailing.equalTo(-4)
            make.height.equalTo(73.DScale)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(giftImageView.snp.bottom).offset(-2)
            make.height.equalTo(18.DScale)
            make.leading.trailing.equalToSuperview()
        }
        
        
        priceLabel.snp.makeConstraints { make in
            make.height.equalTo(15.DScale)
        }
        
        gemsIcon.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 10, height: 10).DScale)
        }
        
        stackView = hstack(gemsIcon, priceLabel, spacing: 3, alignment: .center)
        stackView.snp.makeConstraints { make in
                make.top.equalTo(nameLabel.snp.bottom).offset(2)
                make.height.equalTo(15.DScale)
                make.centerX.equalToSuperview()
            }
        
        sendButton.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(24.DScale)
        }
        
    }
    
    @objc private func sendAction(_ sender: UIGestureRecognizer)  {
        guard isSelected else { return }
        sendBlock?()
    }
    
    
    func configGiftModel(_ model: LiveGiftItem)  {
        if let imgName = model.icon {
            giftImageView.kf.setImage(with: URL(string: imgName))
        }
        nameLabel.text = model.name ?? ""
        
        switch model.type {
        case .gems:
            gemsIcon.isHidden = false
            if let price = model.price {
                priceLabel.text = "\(price)"
            }
        case .currency:
            gemsIcon.isHidden = true
            if let price = model.price {
                priceLabel.text = "$\(price)"
            }
        default:
            break
        }
    }
    
}
