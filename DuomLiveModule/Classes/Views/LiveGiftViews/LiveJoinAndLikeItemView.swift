//
//  LiveJoinAndLikeItemView.swift
//  DuomLiveModule
//
//  Created by kuroky on 2023/2/14.
//

import Foundation
import DuomBase


class LiveJoinAndLikeItemContainerView: BaseView {
    var datasQueue: [Chat] = []
    
    private var firstItemView: LiveJoinAndLikeItemView = LiveJoinAndLikeItemView()

    private var secondItemView: LiveJoinAndLikeItemView = LiveJoinAndLikeItemView()
    
    private var firstCenterY = (22 / 2).DScale
    
    private var secondCenterY = (22 / 2 + 22).DScale
    
    private var timer: Timer? = nil
    
    override func setupUI() {
        addSubview(firstItemView)
        addSubview(secondItemView)
        
        firstItemView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.height.equalTo(22.DScale)
            make.centerY.equalTo(firstCenterY)
        }

        secondItemView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.height.equalTo(22.DScale)
            make.centerY.equalTo(secondCenterY)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    
        clipsToBounds = true
        firstCenterY = bounds.height / 2
        secondCenterY = bounds.height + bounds.height / 2
    }
    
    deinit {
        timer?.invalidate()
        timer = nil
    }
    
    // 追加数据
    func applyDatas(_ datas: [Chat])  {
        datasQueue.append(contentsOf: datas)
    }
    
    func showItem()  {
        guard datasQueue.isNotEmpty else {
            return
        }
        
        if datasQueue.count == 1 {
            stopTimer()
            self.firstItemView.data = self.datasQueue.first
            animatedSingleItemView()
            return
        }
        
        self.firstItemView.data = self.datasQueue.first
        
        if let timer = timer, timer.isValid {
            return
        } else {
            stopTimer()
            timer = Timer.schedule(repeatInterval: 2, handler: { [weak self] _ in
                guard let self = self else { return }
                print("......>>>>> \(self.datasQueue)")
                guard self.datasQueue.isNotEmpty else {
                    self.stopTimer()
                    return
                }
                
                var firstText: Chat?
                var secondText: Chat?
                
                if self.datasQueue.count == 1 {
                    firstText = self.datasQueue.first
                    secondText = nil
                } else {
                    firstText = self.datasQueue.first
                    secondText = self.datasQueue[1]
                }
                
                self.startAnimation(firstText: firstText, secondText: secondText)
                self.datasQueue.removeFirst()
            })
        }
        
    }
    
    public func startAnimation(firstText: Chat?, secondText: Chat?) {
        firstItemView.data = firstText
        secondItemView.data = secondText
        firstItemView.isHidden = firstText == nil
        secondItemView.isHidden = secondText == nil
        
        firstItemView.center.y = firstCenterY
        secondItemView.center.y = secondCenterY
        
        self.firstItemView.snp.updateConstraints { make in
            make.centerY.equalTo(self.firstCenterY)
        }

        self.secondItemView.snp.updateConstraints { make in
            make.centerY.equalTo(self.secondCenterY)
        }
        
        layoutIfNeeded()
        
        self.firstItemView.snp.updateConstraints { make in
            make.centerY.equalTo(-self.firstCenterY)
        }

        self.secondItemView.snp.updateConstraints { make in
            make.centerY.equalTo(self.firstCenterY)
        }
                                 
        UIView.animate(
            withDuration: 0.3,
            animations: {
                self.firstItemView.center.y = -self.firstCenterY
                self.secondItemView.center.y = self.firstCenterY
                self.layoutIfNeeded()
            },
            completion: { _ in
                swap(&self.firstItemView, &self.secondItemView)
                self.firstItemView.center.y = self.firstCenterY
                self.secondItemView.center.y = self.secondCenterY
            }
        )
    }
    
    func animatedSingleItemView()  {
        UIView.animate(withDuration: 0.3) {
            self.firstItemView.center.y = self.firstCenterY
        } completion: { _ in
            delay(2) {
                self.hideSingleItemView()
            }
        }
    }
    
    func hideSingleItemView()  {
        UIView.animate(withDuration: 0.3) {
            self.firstItemView.center.y = -self.firstCenterY
        } completion: { _ in
            self.firstItemView.isHidden = true
            self.secondItemView.isHidden = true
            self.firstItemView.center.y = self.firstCenterY
            self.secondItemView.center.y = self.secondCenterY
        }
    }
    
    private func stopTimer()  {
        timer?.invalidate()
        timer = nil
    }
}

class LiveJoinAndLikeItemView: BaseView {
    private let iconButton = DuomThemeButton.init(style: .highLight, title: "DUOM", font: .appFont(ofSize: 10, fontType: .Inter_Blod)).then {
        $0.isUserInteractionEnabled = false
    }
    
    private let nameLabel: UILabel = UILabel().then {
        $0.textColor = .themeColor
        $0.font = .appFont(ofSize: 13)
        $0.textAlignment = .left
    }
    
    private let descLabel: UILabel = UILabel().then {
        $0.textColor = .white
        $0.font = .appFont(ofSize: 13)
        $0.textAlignment = .left
    }
    
    var data: Chat? {
        didSet {
            nameLabel.text = data?.name
            descLabel.attributedText = data?.content
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundColor = UIColor(0, 0, 0, 0.2)
        roundCorners(radius: bounds.height / 2)
    }
    
    override func setupUI() {
        super.setupUI()
        
        addSubview(iconButton)
        addSubview(nameLabel)
        addSubview(descLabel)
        
        iconButton.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(5)
            make.size.equalTo(CGSize(width: 42, height: 12).DScale)
        }

        descLabel.snp.makeConstraints { make in
            make.trailing.equalTo(-8)
            make.centerY.equalToSuperview()
            make.height.equalTo(18)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(iconButton.snp.trailing).offset(3)
            make.trailing.equalTo(descLabel.snp.leading).offset(-3)
            make.width.lessThanOrEqualTo(80.DScale)
        }
    }
}


class LiveScrollItemContainerView: BaseView {
    var datasQueue: [Chat] = []

    private let itemView: LiveJoinAndLikeItemView = LiveJoinAndLikeItemView()

    private(set) var isPlaying: Bool = false

    override func setupUI() {
        addSubview(itemView)
        itemView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.height.equalTo(22.DScale)
            make.top.equalTo(self.snp.bottom)
        }

    }

    override func layoutSubviews() {
        super.layoutSubviews()

        clipsToBounds = true
    }
    
    // 追加数据
    func applyDatas(_ datas: [Chat])  {
        datasQueue.append(contentsOf: datas)
    }

    func showItem() {
        guard datasQueue.isNotEmpty else { return }

        if isPlaying {
            return
        } else {
            startAnimation(data: datasQueue.first)
        }
    }



    func startAnimation(_ duration: Double? = 2, data: Chat?)  {
        print("......<<<<<<<<<< \(datasQueue)")
        guard let data = data else {
            isPlaying = false
            return
        }
        isPlaying = true
        itemView.data = data

        let transY: CGFloat = -22.DScale

        UIView.animate(withDuration: 0.25) {
            self.itemView.transform = CGAffineTransform(translationX: 0, y: transY)
        }


        delay(TimeInterval(duration!)) {
            UIView.animate(withDuration: 0.25) {
                self.itemView.transform = self.itemView.transform.translatedBy(x: 0, y: transY)
                self.itemView.alpha = 0
            } completion: { _ in
                self.itemView.transform = self.itemView.transform.translatedBy(x: 0, y: -2 * transY)
                self.itemView.alpha = 1
                self.itemView.isHidden = false
                self.datasQueue.removeFirst()
                self.startAnimation(data: self.datasQueue.first)
            }
        }
    }

}
