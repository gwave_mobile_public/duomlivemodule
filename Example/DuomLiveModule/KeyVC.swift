//
//  KeyVC.swift
//  DuomLiveModule_Example
//
//  Created by kuroky on 2023/2/15.
//  Copyright © 2023 CocoaPods. All rights reserved.
//

import Foundation
import DuomBase

//class KeyboardVC: UIViewController, UITextFieldDelegate {
//    typealias InputComletion = ((String) -> Void)
//
//    private let textField: UITextField = UITextField()
//
//    private lazy var  sendButton = DuomThemeButton.init(style: .highLight, title: buttonTitle)
//
//    private let containerView = UIView().then {
//        $0.backgroundColor = .themeBlackColor
//    }
//
//    var placehloder: String = "Your Comment..."
//
//    var buttonTitle: String = "Send"
//
//    var completion: InputComletion?
//
//    convenience init(_ buttonTitle: String, placehloder: String, inputCompletion: @escaping InputComletion) {
//        self.init(nibName: nil, bundle: nil)
//        self.placehloder = placehloder
//        self.buttonTitle = buttonTitle
//        self.completion = inputCompletion
//        setSubViewsConstraints()
//    }
//
//    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
//        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        textField.becomeFirstResponder()
////        IQKeyboardManager.shared.enable = false
//    }
//
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        textField.resignFirstResponder()
////        IQKeyboardManager.shared.enable = true
//    }
//
//    // 不在setupConstraints写 为了解决键盘问题
//    private func setSubViewsConstraints()  {
//        textField.delegate = self
//        view.backgroundColor = .clear
//        self.view.addSubview(containerView)
//        containerView.addSubview(textField)
//        containerView.addSubview(sendButton)
//
//        containerView.snp.makeConstraints { make in
//            make.leading.trailing.equalToSuperview()
//            make.top.equalTo(self.view.snp.bottom)
//            make.width.equalTo(UIScreen.screenWidth)
//            make.height.equalTo(60)
//        }
////        containerView.isHidden = true
//
//        textField.snp.makeConstraints { make in
//            make.leading.equalTo(16)
//            make.top.equalTo(5)
//            make.trailing.equalTo(sendButton.snp.trailing).offset(-10)
//            make.bottom.equalTo(-5)
//        }
//
//        sendButton.snp.makeConstraints { make in
//            make.trailing.equalTo(-16)
//            make.centerY.equalToSuperview()
//            make.size.equalTo(CGSize(width: 80, height: 40))
//        }
//
//        RxKeyboard.instance.visibleHeight
//            .drive(onNext: { [weak self] keyboardVisibleHeight in
//                guard let self = self else { return }
//                if keyboardVisibleHeight > 0 {
//                    self.containerView.snp.remakeConstraints { make in
//                        make.leading.trailing.equalToSuperview()
//                        make.bottom.equalTo(-keyboardVisibleHeight)
//                        make.width.equalTo(UIScreen.screenWidth)
//                        make.height.equalTo(60)
//                    }
//
//                    self.view.setNeedsLayout()
//
//                    UIView.animate(withDuration: 0.25) {
////                        self.containerView.isHidden = false
//                        self.view.layoutIfNeeded()
//                    }
//                } else {
//                    self.containerView.snp.remakeConstraints { make in
//                        make.leading.trailing.equalToSuperview()
//                        make.top.equalTo(self.view.snp.bottom)
//                        make.width.equalTo(UIScreen.screenWidth)
//                        make.height.equalTo(60)
//                    }
//
//                    self.view.setNeedsLayout()
//
//                    UIView.animate(withDuration: 0.25) {
////                        self.containerView.isHidden = true
//                        self.view.layoutIfNeeded()
//                    }
//                }
//            })
//            .disposed(by: disposeBag)
//    }
//
//    override func setupConstraints() {
//        super.setupConstraints()
//
//        sendButton.rx.tap
//            .subscribe(onNext: { [weak self] in
//                guard let self = self else { return }
//                self.complectionCallback()
//            })
//            .disposed(by: disposeBag)
//    }
//
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        dismiss()
//    }
//
//    private func dismiss()  {
//        textField.resignFirstResponder()
//        self.dismiss(animated: true)
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        view.backgroundColor = .white
//
//        view.addSubview(textField)
//        textField.snp.makeConstraints { make in
//            make.center.equalToSuperview()
//            make.size.equalTo(CGSize(width: 300, height: 80))
//        }
//        textField.delegate = self
//        textField.backgroundColor = .purple
//
//        textField.inputView = TestInputView(frame: CGRect(x: 0, y: 0, width: UIScreen.screenWidth, height: 324))
//    }
//
//
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        textField.becomeFirstResponder()
//    }
//
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        textField.resignFirstResponder()
//    }
//
//
//
//}
//
//
//class TestInputView: BaseView {
//    private let containerView: UIView = UIView().then {
//        $0.backgroundColor = .red
//    }
//    override func setupUI() {
//        super.setupUI()
//        addSubview(containerView)
//        containerView.snp.makeConstraints { make in
//            make.edges.equalToSuperview()
//        }
//    }
//
//
//}
//
