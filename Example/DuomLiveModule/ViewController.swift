//
//  ViewController.swift
//  DuomLiveModule
//
//  Created by kuroky on 09/20/2022.
//  Copyright (c) 2022 kuroky. All rights reserved.
//

import UIKit
import DuomLiveModule
import DuomBase
import RxCocoa
import RxSwift

class ViewController: UIViewController {
    
    let brocaster = UIButton().then {
        $0.backgroundColor = .red
        $0.setTitle("主播", for: .normal)
    }
    
    let audience = UIButton().then {
        $0.backgroundColor = .red
        $0.setTitle("观众", for: .normal)
    }
    
    
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.vstack(brocaster, audience, spacing: 100, alignment: .fill, distribution: .fillEqually)
            .snp.makeConstraints { make in
                make.center.equalToSuperview()
                make.leading.equalTo(50)
                make.trailing.equalTo(-50)
                make.height.equalTo(400)
            }
        
        
        brocaster.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
//                let vc = LiveMainViewController().then {
//                    $0.viewModel = LiveViewModel(role: .Broadcaster, liveMode: .livingMode, userId: UserDefaults.standard.string(forKey: "userId") ?? "", eventId: "2022102004274972300147")
//                }
//                self.navigationController?.pushViewController(vc, animated: true)
                DuomLiveManager.openLiveRoom(eventId: "2022102004274972300147", role: LiveRole.Broadcaster, parentNavigator: self.navigationController)
            })
            .disposed(by: disposeBag)
        
        audience.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
//                let vc = LiveAudienceMainViewController().then {
//                    $0.viewModel = LiveViewModel(role: .Audience, liveMode: .livingMode, userId: UserDefaults.standard.string(forKey: "userId") ?? "", eventId: "2022102004274972300147")
//                }
//                self.navigationController?.pushViewController(vc, animated: true)
                DuomLiveManager.openLiveRoom(eventId: "2022102004274972300147", role: LiveRole.Audience, parentNavigator: self.navigationController)
            })
            .disposed(by: disposeBag)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let v = KeyboardVC()
//        self.navigationController?.pushViewController(v, animated: true)
    }
   
}



class TestVC: UIViewController {
    let containerView = LiveGiftTransformContainerView()
    let scrollView = LiveJoinAndLikeItemContainerView()
    
    let textSview = LiveScrollItemContainerView()
    
    override func viewDidLoad() {
        self.view.backgroundColor = .white
        
        view.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview()
//            make.size.equalTo(CGSize(width: 235.DScale, height: 56))
            make.height.equalTo(56)
            make.width.equalTo(235)
        }
        
//        view.addSubview(scrollView)
//        scrollView.snp.makeConstraints { make in
//            make.leading.equalTo(16)
//            make.height.equalTo(22.DScale)
//            make.width.lessThanOrEqualTo(225.DScale)/
//            make.top.equalTo(containerView.snp.bottom).offset(90)
//        }
        
        view.addSubview(textSview)
        textSview.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.height.equalTo(22.DScale)
            make.width.lessThanOrEqualTo(225.DScale)
            make.top.equalTo(containerView.snp.bottom).offset(90)
        }
        
    }
    
    deinit {
        containerView.cancelAnimation()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true)
        
//        containerView.showGift()
        
//        scrollView.showItem()
        
        textSview.showItem()
    }
}
