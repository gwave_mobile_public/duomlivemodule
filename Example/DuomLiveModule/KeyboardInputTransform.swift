////
////  KeyboardInputTransform.swift
////  HJSwift
////
////  Created by PAN on 2019/3/19.
////  Copyright © 2019 YR. All rights reserved.
////
//
//import Foundation
//import Typist
//import UIKit
//
//private class KeyboardObservable {
//    var minPadding: CGFloat = 0
//    private weak var hostView: UIView?
//    private let typist = Typist()
//    private var lastKeyboardFrame: CGRect = .zero
//    private var rawTransform: CGAffineTransform = .identity
//
//    deinit {
//        typist.stop()
//    }
//
//    required init(hostView: UIView) {
//        self.hostView = hostView
//
//        typist
//            .on(event: .willShow) { [weak self] options in
//                self?.rawTransform = hostView.transform
//                self?.onUpdate(options: options)
//            }
//            .on(event: .didChangeFrame, do: { [weak self] options in
//                self?.onUpdate(options: options)
//            })
//            .on(event: .willHide) { [weak self] options in
//                self?.onUpdate(options: options)
//                self?.rawTransform = .identity
//            }
//            .start()
//    }
//
//    private func onUpdate(options: Typist.KeyboardOptions) {
//        guard let hostView = hostView, options.endFrame.minY != lastKeyboardFrame.minY else { return }
//        lastKeyboardFrame = options.endFrame
//
//        let hostViewMaxY = hostView.convert(CGPoint(x: 0, y: hostView.bounds.maxY), to: nil).y
//        let diff = lastKeyboardFrame.minY - (hostViewMaxY + minPadding)
//
//        UIView.animate(withDuration: options.animationDuration, animations: {
//            if diff < 0 {
//                hostView.transform = self.rawTransform.translatedBy(x: 0, y: diff)
//            } else {
//                hostView.transform = self.rawTransform
//            }
//        })
//    }
//}
//
//private var keyboardObservableKey: Void?
//
//public protocol KeyboardInputTransform {}
//
//public extension KeyboardInputTransform where Self: UIView {
//    func bindTransformWhenInput(padding: CGFloat) {
//        if objc_getAssociatedObject(self, &keyboardObservableKey) as? KeyboardObservable == nil {
//            let newValue = KeyboardObservable(hostView: self)
//            objc_setAssociatedObject(self, &keyboardObservableKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//        }
//        (objc_getAssociatedObject(self, &keyboardObservableKey) as? KeyboardObservable)?.minPadding = padding
//    }
//}
//
//extension UIView: KeyboardInputTransform {}
