//
//  LoginApi.swift
//  DuomChatRoom_Example
//
//  Created by kuroky on 2022/10/8.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import DuomNetworkKit
import Moya
import Alamofire

let loginChatProvider = MoyaProvider<LoginChatApi>(plugins: NetworkConfig.plugins)

enum LoginChatApi {
    case getIMSign
    case bindIM(imUid: String)
    
    // 开启活动
    case startEvent(eventId: String)
}

extension LoginChatApi: NetworkAPI {
    var parameters: [String : Any] {
        return [:]
    }
    
    var urlParameters: [String : Any] {
        switch self {
        case .getIMSign:
            return [:]
        case .bindIM(let imUid):
            return ["imUid": imUid]
        default:
            return [:]
        }
    }
    
    var path: String {
        switch self {
        case .getIMSign:
            return "/v1/customer/im/sign"
        case .bindIM:
            return "/v1/customer/im/bind"
        case let .startEvent(eventId: eventId):
            return "/v1/celebrity/event/start/\(eventId)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getIMSign:
            return .get
        case .bindIM:
            return .post
        default:
            return .post
        }
    }
    
//    var task: Task {
//        switch self {
//        case .getIMSign:
//            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
//        case let .bindIM(imUid: imUid):
//            let params = ["imUid": imUid]
//            return .requestCompositeParameters(bodyParameters: parameters, bodyEncoding: JSONEncoding.default, urlParameters: params)
//        }
//    }
    
}


