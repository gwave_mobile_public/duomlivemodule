//
//  LoginLiveModel.swift
//  DuomLiveModule_Example
//
//  Created by kuroky on 2022/10/12.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import DuomNetworkKit

struct LoginLiveModel: JSONCodable {
    var imUid: String?
    var sign: String?
}
