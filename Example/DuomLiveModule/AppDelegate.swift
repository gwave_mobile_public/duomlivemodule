//
//  AppDelegate.swift
//  DuomLiveModule
//
//  Created by kuroky on 09/20/2022.
//  Copyright (c) 2022 kuroky. All rights reserved.
//

import UIKit
import KKIMModule
import DuomBase
import DuomLiveModule
import DuomNetworkKit
import RxCocoa
import RxSwift
//import IQKeyboardManagerSwift

let USERID_KEY = "userId"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // Agora
//    let AgoraAppId: String = "7c939bd01ad54984978ad725c58e2db1"
    
    // Token deviceID
    // listener
//    let device_id = "00008110-0018653611E1801E"
//    let JWT_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjY1MzY4Njg3MDg2LCJkZXZpY2VJZCI6ImZjZmM3ZTJlNTg2MzUzNjAiLCJpcCI6IjguMjE5Ljc2LjE0NyIsImxvY2FsZSI6InpoIiwibG9naW5UeXBlIjoiMSIsInB1c2hUb2tlbiI6InRva2VuIiwicHVzaFRva2VuVHlwZSI6ImFuZHJvaWQiLCJ0aW1lIjoxNjY1OTczNDg3MDg2LCJ1c2VyQWdlbnQiOiJBcGFjaGUtSHR0cENsaWVudC80LjUuMTMgKEphdmEvMTEuMC4xNCkiLCJ1c2VyX2lkIjoiMjAyMjEwMDgxMDMyMzA2MDEwMTAwMSJ9.xDSz6PmcZOcAFTvU-jrf5r9WVc1G-79Npr_lMg2V5dg"
    
    
    // Owner:
    let device_id = "00008110-00184CC234A1401E"
    let JWT_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjY2MTYyODE1ODg0LCJkZXZpY2VJZCI6IjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMCIsImlwIjoiNDcuMjQzLjEzNC42NSIsImxvY2FsZSI6InpoIiwibG9naW5UeXBlIjoiMSIsInB1c2hUb2tlbiI6InRva2VuIiwicHVzaFRva2VuVHlwZSI6ImlvcyIsInRpbWUiOjE2NjY3Njc2MTU4ODQsInVzZXJBZ2VudCI6IkR1b20vMjAyMjEwMTkxMTAwIENGTmV0d29yay8xMzMzLjAuNCBEYXJ3aW4vMjEuNS4wIiwidXNlcl9pZCI6IjIwMjIxMDE0MDcwODI1NDU4MDIwMDYifQ.mggTqvYdu-d584xoBLKN_yg-yfik7_81U4eCuaEni3A"


    private let disposeBag = DisposeBag()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        window?.makeKeyAndVisible()
        let nav = UINavigationController(rootViewController: ViewController())
        window?.rootViewController = nav
        
        // Keyboard
//        IQKeyboardManager.shared.do {
//            $0.enable = true
//            $0.enableAutoToolbar = false
//            $0.shouldResignOnTouchOutside = true
//            $0.toolbarDoneBarButtonItemText = "Done"
//        }

        initIMSDK()
        initLogin()
        return true
    }
    
    // 获取签名--> 登陆IM--> 绑定Im用户
    private func initLogin()  {
        // NetWork
        NetworkConfig.setupDefault(header: ["device_id": device_id,
                                            "JWT_TOKEN": JWT_TOKEN])
        
        loginChatProvider.rx
            .request(.getIMSign)
            .map(HandyDataModel<LoginLiveModel>.self)
            .compactMap { $0.obj }
            .subscribe(onSuccess: { [weak self] model in
                guard let imUid = model.imUid, let sign = model.sign, let self = self else { return }
                self.loginIm(imUid, sign: sign)
                UserDefaults.standard.set(imUid, forKey: USERID_KEY)
                UserDefaults.standard.synchronize()
            })
            .disposed(by: disposeBag)
    }
    
    // init im
    private func initIMSDK()  {
        DuomHelper.sharedInstance().initIMSDK(20000947)
    }
    
    // login im
    private func loginIm(_ uid: String, sign: String)  {
        DuomHelper.sharedInstance().loginIMSDK(uid, userSig: sign) { [weak self] in
            guard let self = self else { return }
            Logger.log(message: "IM SDK Login Success", level: .info)
            loginChatProvider.rx
                .request(.bindIM(imUid: uid))
                .mapJSON()
                .subscribe(onSuccess: { json in
                    print("\(json)")
                })
                .disposed(by: self.disposeBag)
        } fail: { code, desc in
            Logger.log(message: "IM SDK Login Failure:\(code)------------\(desc)", level: .error)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

