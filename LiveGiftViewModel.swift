//
//  LiveGiftViewModel.swift
//  DuomLiveModule
//
//  Created by kuroky on 2023/2/20.
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa
import DuomNetworkKit

public class LiveGiftViewModel: ViewModel {
    public enum Action: ResponderChainEvent {
        case getGiftList
    }
    
    public class State: Output {
        var joinMsgData: PublishRelay<[Chat]> = .init()
        
        required public init() {}
    }
    
    public func mutate(action: Action, state: State) {
        switch action {
        case .getGiftList:
            getGiftAction()
        }
    }
    
}

extension LiveGiftViewModel {
    private func getGiftAction()  {
        
    }
}
