# DuomLiveModule

[![CI Status](https://img.shields.io/travis/kuroky/DuomLiveModule.svg?style=flat)](https://travis-ci.org/kuroky/DuomLiveModule)
[![Version](https://img.shields.io/cocoapods/v/DuomLiveModule.svg?style=flat)](https://cocoapods.org/pods/DuomLiveModule)
[![License](https://img.shields.io/cocoapods/l/DuomLiveModule.svg?style=flat)](https://cocoapods.org/pods/DuomLiveModule)
[![Platform](https://img.shields.io/cocoapods/p/DuomLiveModule.svg?style=flat)](https://cocoapods.org/pods/DuomLiveModule)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DuomLiveModule is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'DuomLiveModule'
```

## Author

kuroky, kai.he@duom.com

## License

DuomLiveModule is available under the MIT license. See the LICENSE file for more info.
